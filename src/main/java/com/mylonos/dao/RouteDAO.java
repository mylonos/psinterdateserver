package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.dto.RoutesDTO;
import com.mylonos.psinter.domain.models.Route;
import java.util.List;

/**
 * Created by manolis on 26-Mar-16.
 */
public interface RouteDAO {

    List<Route> getRoutes();

    List<Route> getRoutes(String user_id, long date, long date_until);

    Results getRoutesByVehicle(String vehicleId, Long date, Long date_until);

    List<Route> getRoutes(String q);

    void createRoute(String q);

    void updateRoute(String q);

    void insertRoutes(RoutesDTO routesDTO, String vehicleId);

    Route getRoutesByRouteId(String routeId);

    Results getRoutesInfoByVehicle(String vehicleId, Long date, Long dateUntil);

}
