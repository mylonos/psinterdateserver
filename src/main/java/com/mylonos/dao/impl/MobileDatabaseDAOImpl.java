package com.mylonos.dao.impl;

import com.mylonos.dao.MobileDatabaseDAO;
import com.mylonos.dao.UserDAO;
import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import com.mylonos.mappers.MobileDatabaseMapper;
import com.mylonos.mappers.UserMapper;
import com.mylonos.psinter.domain.models.MobileDatabase;
import com.mylonos.psinter.domain.models.User;
import com.mylonos.utils.PasswordHash;
import com.mylonos.utils.Queries;
import com.mylonos.utils.Utils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Types;
import java.util.Date;
import java.util.List;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class MobileDatabaseDAOImpl implements MobileDatabaseDAO {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private PasswordHash passwordHash;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public MobileDatabase getmobileDatabase() {
        return jdbcTemplateObject.queryForObject(Queries.GET_MOBILE_DATABASE, new MobileDatabaseMapper());

    }


}
