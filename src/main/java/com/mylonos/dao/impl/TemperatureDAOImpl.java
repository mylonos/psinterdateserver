/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mylonos.dao.impl;

import com.mylonos.dao.TemperatureDAO;
import com.mylonos.mappers.TemperatureMapper;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Temperature;
import com.mylonos.utils.PasswordHash;
import com.mylonos.utils.Utils;
import java.sql.Timestamp;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author manolis
 */
@Component
public class TemperatureDAOImpl implements TemperatureDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PlatformTransactionManager transactionManager;

    private JdbcTemplate jdbcTemplateObject;
    private PasswordHash passwordHash;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        passwordHash = new PasswordHash();

    }

    public void setTransactionManager(PlatformTransactionManager txManager) {

        this.transactionManager = txManager;

    }

    @Override
    public Temperature getLastTemperatureByVehicleId(String vehicleId) {

        String queryTemperaturesForDates = "SELECT * FROM temperature_humidity where vehicle_id=? "
                + " order by date_time desc limit 1;"
                + ";";
        List<Temperature> temperatures = jdbcTemplateObject.query(queryTemperaturesForDates, new Object[]{vehicleId},
                new TemperatureMapper());
        return temperatures.get(0);

    }

    @Override
    public Results getTemperaturesForVehicle(String vehicleId, Long date, Long dateUntil) {
        Results results = new Results();
        results.setTotal(0);

        if (date == null) {
            date = 0L;
        }

        if (dateUntil == null) {
            dateUntil = System.currentTimeMillis();
        }
        System.out.println(new Timestamp(date));
        System.out.println(new Timestamp(dateUntil));
        String queryTemperaturesForDates = "SELECT * FROM temperature_humidity\n"
                + "\n"
                + "where vehicle_id=? and (date_time BETWEEN ? AND ?)\n"
                + "\n"
                + ";";
        List<Temperature> temperatures = jdbcTemplateObject.query(queryTemperaturesForDates, new Object[]{vehicleId,
            new Timestamp(date), new Timestamp(dateUntil)}, new TemperatureMapper());

        results.setTotal(temperatures.size());
        results.setResults(temperatures);

        return results;
    }

}
