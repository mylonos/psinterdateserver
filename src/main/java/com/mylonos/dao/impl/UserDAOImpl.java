package com.mylonos.dao.impl;

import com.mylonos.dao.UserDAO;
import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.dto.UserCurrentPositionDTO;
import com.mylonos.psinter.domain.dto.UserDTO;
import com.mylonos.mappers.MobileUserMapper;
import com.mylonos.mappers.UserCurrentPositionDTOMapper;
import com.mylonos.mappers.UserDTOMapper;
import com.mylonos.mappers.UserMapper;
import com.mylonos.psinter.domain.models.MobileUser;
import com.mylonos.psinter.domain.models.User;
import com.mylonos.utils.PasswordHash;
import com.mylonos.utils.Queries;
import com.mylonos.utils.Utils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class UserDAOImpl implements UserDAO {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private PasswordHash passwordHash;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        passwordHash = new PasswordHash();
    }

    @Override
    public List<User> findByUsername(String s) {
        String findByUsername = "select * from user";
        List<User> listContact = jdbcTemplateObject.query(findByUsername, new UserMapper());
        return listContact;

    }

    @Override
    public List<UserDTO> getUsers(String s) {
        String findByUsername = "select * from user ";
        return jdbcTemplateObject.query(findByUsername, new UserDTOMapper());

    }

    @Override
    public String getUsername(String username) {
        return "hello " + username + " !!!!";
    }

    @Override
    public List<UserCurrentPositionDTO> getUsersCurrentPosition() {
        String findByUsername = " SELECT * FROM user_current_position,user where user.user_id=user_current_position.user_id;";
        return jdbcTemplateObject.query(findByUsername, new UserCurrentPositionDTOMapper());

    }

    @Override
    public AuthenticationDTO authenticate(String username, String password) {
        try {
            User user = jdbcTemplateObject.queryForObject(Queries.AUTHENTICATE_USER_QUERY, new Object[]{username}, new UserMapper());
            if (user != null) {

                try {
//                    if (passwordHash.validatePassword(password, user.getPassword())) {
                    if (password.equals(user.getPassword())) {
                        user.setToken(createAndStoreJWT(user));
                        return new AuthenticationDTO(true, "", user.setPassword(""));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new AuthenticationDTO();
    }

    @Override
    public boolean isValidToken(String token) {
        return false;
    }

    private String createAndStoreJWT(User user) {
// define SQL types of the arguments
        int[] types = {Types.VARCHAR, Types.VARCHAR};

        String token = Jwts.builder().setSubject(user.getUsername())
                .claim("roles", user.getType()).setIssuedAt(new Date()).setIssuer("mylonos").setId(user.getUserId())
                .claim("fisrtName", user.getFirstName())
                .claim("lastName", user.getLastName())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        jdbcTemplateObject.update(Queries.INSERT_TOKEN_FOR_USER_QUERY, new Object[]{token, user.getUserId()}, types);
        return token;
    }

    @Override
    public List<MobileUser> findByUsernameMobile(String s) {
        String findByUsername = "select * from user where is_active=1";
        List<MobileUser> listContact = jdbcTemplateObject.query(findByUsername, new MobileUserMapper());
        return listContact;
    }

    @Override
    public Results findByEmployeeByStore(String store, Integer isEmployee, Integer isActive) {
        Results results = new Results();

        String findByUsername = "select * from user where store=? and is_employee=? and is_active=?;";
        List<User> listContact = jdbcTemplateObject.query(findByUsername, new Object[]{store, isEmployee, isActive}, new UserMapper());
        results.setCurrentPage(0);
        results.setResults(listContact);
        results.setTotal(listContact.size());
        return results;

    }

    @Override
    public void createUser(User user) {

        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
            Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR};

        String insrtUser = "INSERT INTO user\n"
                + "(`user_id`,\n"
                + "`first_name`,\n"
                + "`last_name`,\n"
                + "`username`,\n"
                + "`password`,\n"
                + "`token`,\n"
                + "`type`,\n"
                + "`store`,\n"
                + "`is_employee`,\n"
                + "`is_active`,\n"
                + "`search_field`,\n"
                + "`email`)\n"
                + "VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?);";

        jdbcTemplateObject.update(insrtUser, new Object[]{user.getUserId(), user.getFirstName(), user.getLastName(),
            user.getUsername(), user.getPassword(), user.getToken(), user.getType(), user.getStore(), user.getIsEmpoyee(),
            user.getIsActive(), user.getSearchField(), user.getEmail()}, types);

    }

    @Override
    public boolean deleteUser(String userId) {

        int[] types = {Types.VARCHAR};

        String insrtUser = "UPDATE user\n"
                + "SET is_active=0 where `user_id`=?\n"
                + ";";

        jdbcTemplateObject.update(insrtUser, new Object[]{userId}, types);

        return true;

    }

    @Override
    public void updateUser(User user) {

        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
            Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};

        String insrtUser = " UPDATE user\n"
                + " SET \n"
                + "`first_name`=?,\n"
                + "`last_name`=?,\n"
                + "`username`=?,\n"
                + "`password`=?,\n"
                + "`token`=?,\n"
                + "`type`=?,\n"
                + "`store`=?,\n"
                + "`is_employee` =?,\n"
                + "`is_active` =?,\n"
                + "`search_field` =?,\n"
                + "`email`=? where `user_id`=?\n";

        jdbcTemplateObject.update(insrtUser, new Object[]{user.getFirstName(), user.getLastName(),
            user.getUsername(), user.getPassword(), user.getToken(), user.getType(), user.getStore(), user.getIsEmpoyee(),
            user.getIsActive(), user.getSearchField(),
            user.getEmail(), user.getUserId()}, types);

    }

}
