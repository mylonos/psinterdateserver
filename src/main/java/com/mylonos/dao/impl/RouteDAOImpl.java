package com.mylonos.dao.impl;

import com.mylonos.dao.RouteDAO;
import com.mylonos.mappers.RouteMapper;
import com.mylonos.mappers.SegmentMapper;
import com.mylonos.mappers.SegmentPointMapper;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.dto.RoutesDTO;
import com.mylonos.psinter.domain.models.Route;
import com.mylonos.psinter.domain.models.RoutePoint;
import com.mylonos.psinter.domain.models.Segment;
import com.mylonos.utils.DateHelper;
import com.mylonos.utils.PasswordHash;
import com.mylonos.utils.Utils;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class RouteDAOImpl implements RouteDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PlatformTransactionManager transactionManager;

    private JdbcTemplate jdbcTemplateObject;
    private PasswordHash passwordHash;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        passwordHash = new PasswordHash();

    }

    public void setTransactionManager(PlatformTransactionManager txManager) {

        this.transactionManager = txManager;

    }

    @Override
    public List<Route> getRoutes() {
        return null;
    }

    @Override
    public List<Route> getRoutes(String user_id, long date, long until) {
//
//        user_id = "2656aaee-3938-4856-af99-3adf5596e962";
//        date = 1463691600000l;
//        until = 1602571600000l;

        String select_route = "select * from  `route_record` where user_id = ? and (date>? and date<=?);";

        String select_segments = " select * from `route_record_segment` where route_record_id=? ;";

        String select_segments_points = "select * from `route_point_record` where route_record_segment_id=? order by timestamp;";

        DateHelper dateHelper = new DateHelper();

        List<Route> routes = jdbcTemplateObject.query(select_route, new Object[]{user_id, date, until}, new RouteMapper());

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        try {
            for (Route route : routes) {
                long now = System.currentTimeMillis();

                List<Segment> segments = jdbcTemplateObject.query(select_segments, new Object[]{route.getRoute_id()}, new SegmentMapper());

                route.setSegments((ArrayList<Segment>) segments);

                for (Segment segment : segments) {

                    segment.setRoutePoints((ArrayList<RoutePoint>) jdbcTemplateObject.query(select_segments_points,
                            new Object[]{segment.getSegment_id()}, new SegmentPointMapper()));
                }

            }
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            transactionManager.rollback(txStatus);
        }

        return routes;
    }

    @Override
    public Results getRoutesByVehicle(String vehicleId, Long date, Long dateUntil) {

        Results results = new Results();
        results.setTotal(0);

        if (date == null) {
            date = 0L;
        }

        if (dateUntil == null) {
            dateUntil = System.currentTimeMillis();
        }

        String select_route = "select * from  `route_record` where vehicle_id = ? and (date>? and date<=?) order by date desc;";

        String select_segments = " select * from `route_record_segment` where route_record_id=? ;";

        String select_segments_points = "select * from `route_point_record` where route_record_segment_id=? order by timestamp;";

        DateHelper dateHelper = new DateHelper();

        List<Route> routes = jdbcTemplateObject.query(select_route, new Object[]{vehicleId, date, dateUntil}, new RouteMapper());

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        try {
            for (Route route : routes) {
                List<Segment> segments = jdbcTemplateObject.query(select_segments, new Object[]{route.getRoute_id()}, new SegmentMapper());
                route.setSegments((ArrayList<Segment>) segments);
                for (Segment segment : segments) {
                    segment.setRoutePoints((ArrayList<RoutePoint>) jdbcTemplateObject.query(select_segments_points,
                            new Object[]{segment.getSegment_id()}, new SegmentPointMapper()));
                }
            }
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            transactionManager.rollback(txStatus);
        }
        results.setTotal(routes.size());
        results.setResults(routes);
        return results;
    }

    @Override
    public Results getRoutesInfoByVehicle(String vehicleId, Long date, Long dateUntil) {

        Results results = new Results();
        results.setTotal(0);

        if (date == null) {
            date = 0L;
        }

        if (dateUntil == null) {
            dateUntil = System.currentTimeMillis();
        }

        String select_route = "select * from  `route_record` where vehicle_id = ? and (date>? and date<=?) order by date desc;";

        List<Route> routes = jdbcTemplateObject.query(select_route, new Object[]{vehicleId, date, dateUntil}, new RouteMapper());
        results.setTotal(routes.size());
        results.setResults(routes);

        return results;
    }

    @Override
    public Route getRoutesByRouteId(String routeId) {

        Results results = new Results();
        results.setTotal(0);

        String select_route = "select * from  `route_record` where route_id = ?;";

        String select_segments = " select * from `route_record_segment` where route_record_id=? ;";

        String select_segments_points = "select * from `route_point_record` where route_record_segment_id=? order by timestamp;";

        DateHelper dateHelper = new DateHelper();

        List<Route> routes = jdbcTemplateObject.query(select_route, new Object[]{routeId}, new RouteMapper());

//        TransactionDefinition txDef = new DefaultTransactionDefinition();
//        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        try {
//            AtomicInteger x =new;
            for (Route route : routes) {
                List<Segment> segments = jdbcTemplateObject.query(select_segments, new Object[]{route.getRoute_id()}, new SegmentMapper());
                route.setSegments((ArrayList<Segment>) segments);
                segments.forEach((segment) -> {
//                    System.out.println(x++);
                    segment.setRoutePoints((ArrayList<RoutePoint>) jdbcTemplateObject.query(select_segments_points,
                            new Object[]{segment.getSegment_id()}, new SegmentPointMapper()));
                });
            }
//            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
//            transactionManager.rollback(txStatus);
        }
        return routes.get(0);
    }

    @Override
    public List<Route> getRoutes(String q) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createRoute(String q) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateRoute(String q) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertRoutes(RoutesDTO routesDTO, String vehicleId) {

        List<Route> routes = routesDTO.getRoutes();

        String insert_route = "INSERT INTO `route_record` (`route_id`, `date`, `desc`, `user_id`, "
                + " `device_id`,vehicle_id) VALUES (?, ?, ?, ?, ?,?);";

        String insert_segments = "INSERT INTO `route_record_segment` (`segment_id`, "
                + "`route_record_id`, `total_in_meter`) VALUES (?, ?, ?);";

        String insert_segments_points = "INSERT INTO `route_point_record` (`route_point_record_id`, "
                + " `route_record_segment_id`, `longitude`, `latitude`, `timestamp`, `data`, "
                + "  `device_id`, `speed`, `bearing`, `accuracy`)"
                + "  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        String delete_route = "delete from route_record where route_id=?";
        String delete_segment = "delete from route_record_segment where route_record_id=?";
        String delete_segment_points = "delete from route_point_record where route_record_segment_id=?";

        int[] types_route = {Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};

        int[] types_segment = {Types.VARCHAR, Types.VARCHAR, Types.DOUBLE};

        int[] types_segment_points = {Types.VARCHAR, Types.VARCHAR, Types.DOUBLE, Types.DOUBLE, Types.BIGINT, Types.VARCHAR,
            Types.VARCHAR, Types.DOUBLE, Types.DOUBLE, Types.DOUBLE};

        int[] types_del = {Types.VARCHAR};

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        try {
            for (Route route : routes) {
                long now = System.currentTimeMillis();

                jdbcTemplateObject.update(delete_route,
                        new Object[]{route.getRoute_id()
                        }, types_del);

                jdbcTemplateObject.update(delete_segment,
                        new Object[]{route.getRoute_id()
                        }, types_del);

                jdbcTemplateObject.update(insert_route,
                        new Object[]{route.getRoute_id(), route.getDate(), route.getDescription(), route.getUser_id(),
                            route.getDevice_id(), vehicleId
                        }, types_route);

                List<Segment> segments = route.getSegments();

                for (Segment segment : segments) {

                    jdbcTemplateObject.update(delete_segment_points,
                            new Object[]{segment.getSegment_id()
                            }, types_del);

                    jdbcTemplateObject.update(insert_segments,
                            new Object[]{segment.getSegment_id(), route.getRoute_id(), segment.getDistence()}, types_segment);

                    List<RoutePoint> routePoints = segment.getRoutePoints();

                    for (RoutePoint routePoint : routePoints) {

                        jdbcTemplateObject.update(insert_segments_points,
                                new Object[]{routePoint.getRoute_point_id(), segment.getSegment_id(), routePoint.getLongitude(),
                                    routePoint.getLatitude(), routePoint.getTimestamp(), routePoint.getData(), routePoint.getDevice_id(),
                                    routePoint.getSpeed(), routePoint.getBearing(), routePoint.getAccuracy()}, types_segment_points);
                    }

                }

            }
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            transactionManager.rollback(txStatus);
        }

    }

}
