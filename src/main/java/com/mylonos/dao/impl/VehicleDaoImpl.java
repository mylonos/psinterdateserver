package com.mylonos.dao.impl;

import com.mylonos.dao.VehicleDao;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.mappers.VehicleMapper;
import com.mylonos.psinter.domain.models.Vehicle;
import com.mylonos.utils.Utils;
import java.sql.Types;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class VehicleDaoImpl implements VehicleDao {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

//    @Override
//    public List<User> findByUsername(String s) {
//        String findByUsername = "select * from user";
//        List<User> listContact = jdbcTemplateObject.query(findByUsername, new UserMapper());
//        return listContact;
//
//    }
//
//    @Override
//    public List<UserDTO> getUsers(String s) {
//        String findByUsername = "select * from user ";
//        return jdbcTemplateObject.query(findByUsername, new UserDTOMapper());
//
//    }
//
//    @Override
//    public String getUsername(String username) {
//        return "hello " + username + " !!!!";
//    }
//
//    @Override
//    public List<UserCurrentPositionDTO> getUsersCurrentPosition() {
//        String findByUsername = " SELECT * FROM user_current_position,user where user.user_id=user_current_position.user_id;";
//        return jdbcTemplateObject.query(findByUsername, new UserCurrentPositionDTOMapper());
//
//    }
//
//    @Override
//    public AuthenticationDTO authenticate(String username, String password) {
//        try {
//            User user = jdbcTemplateObject.queryForObject(Queries.AUTHENTICATE_USER_QUERY, new Object[]{username}, new UserMapper());
//            if (user != null) {
//
//                try {
////                    if (passwordHash.validatePassword(password, user.getPassword())) {
//                    if (password.equals(user.getPassword())) {
//                        user.setToken(createAndStoreJWT(user));
//                        return new AuthenticationDTO(true, "", user.setPassword(""));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return new AuthenticationDTO();
//    }
//
//    @Override
//    public boolean isValidToken(String token) {
//        return false;
//    }
//
//    private String createAndStoreJWT(User user) {
//// define SQL types of the arguments
//        int[] types = {Types.VARCHAR, Types.VARCHAR};
//
//        String token = Jwts.builder().setSubject(user.getUsername())
//                .claim("roles", user.getType()).setIssuedAt(new Date()).setIssuer("mylonos").setId(user.getUserId())
//                .claim("fisrtName", user.getFirstName())
//                .claim("lastName", user.getLastName())
//                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
//
//        jdbcTemplateObject.update(Queries.INSERT_TOKEN_FOR_USER_QUERY, new Object[]{token, user.getUserId()}, types);
//        return token;
//    }
//
//    @Override
//    public List<User> findByUsernameMobile(String s) {
//        String findByUsername = "select * from user";
//        List<User> listContact = jdbcTemplateObject.query(findByUsername, new UserMapper());
//        return listContact;
//    }
//
//    @Override
//    public Results findByEmployeeByStore(String store, Integer isEmployee) {
//        Results results = new Results();
//
//        String findByUsername = "select * from user where store=? and is_employee=?;";
//        List<User> listContact = jdbcTemplateObject.query(findByUsername, new Object[]{store, isEmployee}, new UserMapper());
//        results.setCurrentPage(0);
//        results.setResults(listContact);
//        results.setTotal(listContact.size());
//        return results;
//
//    }
//
//    @Override
//    public void createUser(User user) {
//
//        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
//            Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.VARCHAR};
//
//        String insrtUser = "INSERT INTO user\n"
//                + "(`user_id`,\n"
//                + "`first_name`,\n"
//                + "`last_name`,\n"
//                + "`username`,\n"
//                + "`password`,\n"
//                + "`token`,\n"
//                + "`type`,\n"
//                + "`store`,\n"
//                + "`is_employee`,\n"
//                + "`email`)\n"
//                + "VALUES ( ?,?,?,?,?,?,?,?,?,?);";
//
//        jdbcTemplateObject.update(insrtUser, new Object[]{user.getUserId(), user.getFirstName(), user.getLastName(),
//            user.getUsername(), user.getPassword(), user.getToken(), user.getType(), user.getStore(), user.getIsEmpoyee(),
//            user.getEmail()}, types);
//
//    }
    @Override
    public Results getVehicleByStore(String storeId) {
        Results results = new Results();

        String findByUsername = "select * from vehicle where store=? and deleted=0 ;";
        List<Vehicle> listContact = jdbcTemplateObject.query(findByUsername, new Object[]{storeId}, new VehicleMapper());
        results.setCurrentPage(0);
        results.setResults(listContact);
        results.setTotal(listContact.size());
        return results;

    }

    @Override
    public void createVehicle(Vehicle vehicle) {
        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.DATE, Types.INTEGER,
            Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
        if (null == vehicle.getVehicleId() || vehicle.getVehicleId().trim().isEmpty()) {
            vehicle.setVehicleId(new Utils().getUniqueId());
        }
        String instertVehicle = "INSERT INTO vehicle\n"
                + "(`vehicle_id`,\n"
                + "`plate`,\n"
                + "`first_licence`,\n"
                + "`acquisition_date`,\n"
                + "`status`,\n"
                + "`store`,\n"
                + "`vehicle_description`,\n"
                + "`search_field`)\n"
                + "VALUES ( ?,?,?,?,?,?,?,?);";

        jdbcTemplateObject.update(instertVehicle, new Object[]{vehicle.getVehicleId(), vehicle.getPlate(), vehicle.getFirstLicence(),
            vehicle.getAcquisitionDate(), vehicle.getStatus(), vehicle.getStore(), vehicle.getVehicleDescription(), vehicle.getSearch()}, types);
    }

    @Override
    public void updateVehicle(Vehicle vehicle) {
        int[] types = {Types.VARCHAR, Types.DATE, Types.DATE, Types.INTEGER,
            Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};

        String instertVehicle = "UPDATE vehicle\n"
                + "SET \n"
                + "`plate`=?,\n"
                + "`first_licence`=?,\n"
                + "`acquisition_date`=?,\n"
                + "`status`=?,\n"
                + "`store`=?,\n"
                + "`vehicle_description`=?,\n"
                + "`search_field`=?\n"
                + "where vehicle_id=?;";

        jdbcTemplateObject.update(instertVehicle, new Object[]{vehicle.getVehicleId(), vehicle.getPlate(), vehicle.getFirstLicence(),
            vehicle.getAcquisitionDate(), vehicle.getStatus(), vehicle.getStore(), vehicle.getVehicleDescription(), vehicle.getSearch()}, types);
    }

    @Override
    public void deleteVehicle(String id) {
        int[] types = {Types.INTEGER, Types.VARCHAR};

        String instertVehicle = "UPDATE vehicle\n"
                + "SET \n"
                + "`deleted`=? \n"
                + " where vehicle_id=?;";

        jdbcTemplateObject.update(instertVehicle, new Object[]{1, id}, types);
    }

    @Override
    public Results getVehicleByEmployee(String employeeId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
