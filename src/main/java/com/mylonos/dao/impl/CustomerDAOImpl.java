package com.mylonos.dao.impl;

import com.mylonos.psinter.domain.models.CustomerSimpleInvoices;
import com.mylonos.psinter.domain.models.CustomerSimple;
import com.mylonos.psinter.domain.models.Invoice;
import com.mylonos.psinter.domain.models.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylonos.dao.CustomerDAO;
import com.mylonos.psinter.domain.dto.CustomerDTO;
import com.mylonos.psinter.domain.dto.CustomerStatusDTO;
import com.mylonos.psinter.domain.dto.CustomerVisitDTO;
import com.mylonos.psinter.domain.dto.CustomersOnMapResults;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.mappers.*;
import com.mylonos.utils.Queries;
import com.mylonos.utils.Utils;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class CustomerDAOImpl implements CustomerDAO {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private Utils utils;
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);

    }

    @Override
    public CustomerDTO getAllCustomers(String s) {

//        MapSqlParameterSource namedParams = new MapSqlParameterSource();
//        namedParams.addValue("query", s + "%");
        CustomerDTO customerDTO
                = new CustomerDTO(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_QUERY, new Object[]{"%" + s + "%"}, new CustomerMapper()));
        return customerDTO;

    }

    @Override
    public List<CustomerSimple> getAllCustomersSimple(String s, int limit) {
        MapSqlParameterSource namedParams = new MapSqlParameterSource();
        namedParams.addValue("query", s + "%");
        List<CustomerSimple> customerSimples = jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_QUERY_SIMPLE, new Object[]{"%" + s + "%", limit}, new CustomerSimpleMapper());

        return customerSimples;
    }

    @Override
    public CustomerSimple getCustomerSimple(String id) {
        return null;
    }

    /**
     *
     * @param id
     * @return CustomerSimpleInvoices
     */
    @Override
    public CustomerSimpleInvoices getCustomerSimpleInvoices(String id) {
        CustomerSimpleInvoices customerSimpleInvoices = jdbcTemplateObject.queryForObject(Queries.GET_ALL_CUSTOMER_AND_INVOICES_BY_ID_QUERY_SIMPLE, new Object[]{id}, new CustomerSimpleInvoicesMapper());

        List<String> invoices = jdbcTemplateObject.queryForList(Queries.GET_ALL_CUSTOMER_INVOICES_BY_ID_QUERY, new Object[]{id}, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Invoice> invoiceList = new ArrayList<Invoice>();
        for (String invoice : invoices) {

            try {
                invoiceList.add(objectMapper.readValue(invoice, Invoice.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        customerSimpleInvoices.setInvoices(invoiceList);
        return customerSimpleInvoices;
    }

    @Override
    public void updateCustomerPositions(List<Customer> customers) {

        if (customers.size() == 0) {
            return;
        }

        String update = "UPDATE customer SET longitude = ?,latitude = ?, confirmation_date = ? WHERE customer_id = ?;";

        int[] types_update = {Types.DOUBLE, Types.DOUBLE, Types.BIGINT, Types.VARCHAR};

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        try {
            for (Customer customer : customers) {

                jdbcTemplateObject.update(update,
                        new Object[]{customer.getLongitude(), customer.getLatitude(), customer.getConfirmation_date(),
                            customer.getCustomer_id()}, types_update);
            }

            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            transactionManager.rollback(txStatus);
        }
    }

    @Override
    public CustomerDTO getAllCustomersByCompany(String id, String s) {
        return new CustomerDTO(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_BY_COMPANY_QUERY1, new Object[]{id, "%" + s + "%"},
                new CustomerMapper()));

    }

    @Override
    public HashMap<String, Customer> getCustomerList() {

        HashMap<String, Customer> customers = new HashMap<>();
        List<Map<String, Object>> rows = jdbcTemplateObject.queryForList(Queries.GET_ALL_CUSTOMERS);
        List<Customer> customersw = jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS,
                new BeanPropertyRowMapper<Customer>(Customer.class));
        for (Customer customer : customersw) {
            customers.put(customer.getCustomer_id(), customer);
        }
        return customers;
    }

    @Override
    public CustomersOnMapResults getAllCustomersForCompany(String company) {

        CustomersOnMapResults mapResults = new CustomersOnMapResults();
        if (null == company || company.isEmpty()) {

            mapResults.setUnconfirmend(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_QUERY_UNCONFIRMED, new CustomerForMapMapper()));
            mapResults.setConfirmed(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_QUERY_CONFIRMED, new CustomerForMapMapper()));
            return mapResults;

        }

        mapResults.setStore(company);
        mapResults.setUnconfirmend(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_BY_COMPANY_QUERY_UNCONFIRMED, new Object[]{company}, new CustomerForMapMapper()));
        mapResults.setConfirmed(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_BY_COMPANY_QUERY_CONFIRMED, new Object[]{company}, new CustomerForMapMapper()));

        return mapResults;

    }

    @Override
    public List<CustomerVisitDTO> getAllCustomersForVistis(String user_id, long date, long until) {

        if (user_id == null || user_id.isEmpty()) {
            return jdbcTemplateObject.query(Queries.GET_CUSTOMER_VISITS, new Object[]{user_id, date, until}, new CustomerVistisDTOMapper());
        }

        List<CustomerVisitDTO> customerVisitDTOs = jdbcTemplateObject.query(Queries.GET_CUSTOMER_VISITS, new Object[]{user_id, date, until}, new CustomerVistisDTOMapper());
        List<CustomerVisitDTO> customerVisitDTOs2 = new ArrayList<CustomerVisitDTO>();

        for (CustomerVisitDTO customerVisitDTO : customerVisitDTOs) {
            boolean add = true;
            for (CustomerVisitDTO customerVisitDTO1 : customerVisitDTOs2) {
                if (customerVisitDTO1.getReference().equals(customerVisitDTO.getReference())) {
                    customerVisitDTO1.setDuration(customerVisitDTO1.getDuration() + customerVisitDTO.getDuration());
                    add = false;
                    break;
                }
            }
            if (add) {
                customerVisitDTOs2.add(customerVisitDTO);
            }

        }

        return customerVisitDTOs2;

    }

    @Override
    public CustomerStatusDTO getCustomerStatus(String store) {
        CustomerStatusDTO customerStatusDTO = new CustomerStatusDTO();

        customerStatusDTO.setConfirmed(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_BY_COMPANY_QUERY_CONFIRMED, new Object[]{store},
                new CustomerMapper()));
        customerStatusDTO.setUnconfirmed(jdbcTemplateObject.query(Queries.GET_ALL_CUSTOMERS_BY_COMPANY_QUERY_UNCONFIRMED, new Object[]{store},
                new CustomerMapper()));

        return customerStatusDTO;
    }

    @Override
    public Results getCustomerInvoices(String id, int page, int resultsPerPage) {

        Results customerInvoices = new Results();
        List<String> invoices = jdbcTemplateObject.queryForList(Queries.GET_ALL_CUSTOMER_INVOICES_BY_ID_QUERY, new Object[]{id}, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        List<Invoice> invoiceList = new ArrayList<Invoice>();

        for (String invoice : invoices) {

            try {
                invoiceList.add(objectMapper.readValue(invoice, Invoice.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        customerInvoices.setResults(invoiceList);
        customerInvoices.setTotal(invoiceList.size());
        return customerInvoices;
    }

    @Override
    public List<Customer> listAllCustomers(String s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Customer> listAllCustomersByCompany(String s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer getCustomerById(String s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isertCustomers(CustomerDTO cdto) {

        String insertQuery = "INSERT INTO `customer`\n"
                + "(`customer_id`,\n"
                + "`person_id`,\n"
                + "`sales_person_id`,\n"
                + "`reference`,\n"
                + "`customer_name`,\n"
                + "`customer_alternative_name`,\n"
                + "`customer_type`,\n"
                + "`inactive`,\n"
                + "`remarks`,\n"
                + "`trade_discount`,\n"
                + "`balance_limit`,\n"
                + "`start_date`,\n"
                + "`document_message`,\n"
                + "`activity_type`,\n"
                + "`tax_registration_number`,\n"
                + "`tax_registration_office`,\n"
                + "`vat_status`,\n"
                + "`store`,\n"
                + "`address1`,\n"
                + "`address2`,\n"
                + "`city`,\n"
                + "`postal_code`,\n"
                + "`telephone1`,\n"
                + "`telephone2`,\n"
                + "`other_remarks`,\n"
                + "`search_field`)\n"
                + "VALUES\n"
                + "(?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?) ON DUPLICATE KEY UPDATE person_id=?," + "`sales_person_id`=?,\n"
                + "`reference`=?,\n"
                + "`customer_name`=?,\n"
                + "`customer_alternative_name`=?,\n"
                + "`customer_type`=?,\n"
                + "`inactive`=?,\n"
                + "`remarks`=?,\n"
                + "`trade_discount`=?,\n"
                + "`balance_limit`=?,\n"
                + "`start_date`=?,\n"
                + "`document_message`=?,\n"
                + "`activity_type`=?,\n"
                + "`tax_registration_number`=?,\n"
                + "`tax_registration_office`=?,\n"
                + "`vat_status`=?,\n"
                + "`store`=?,\n"
                + "`address1`=?,\n"
                + "`address2`=?,\n"
                + "`city`=?,\n"
                + "`postal_code`=?,\n"
                + "`telephone1`=?,\n"
                + "`telephone2`=?,\n"
                + "`other_remarks`=?,\n"
                + "`search_field`=?";

        try {
            PreparedStatement ps1 = jdbcTemplateObject.getDataSource().getConnection().prepareStatement(insertQuery);
            List<Customer> customers = (List<Customer>) cdto.getCustomers();
            int x = 0;
            HashMap<String, Integer> d = new HashMap<>();
            HashSet ss = new HashSet();
            for (Customer c : customers) {
                x++;
                d.put(c.getCustomer_id(), 1);
                ss.add(c.getCustomer_id());
//                System.out.println(c.getCustomer_name());
                if (c.getCustomer_id() == null || c.getCustomer_id().isEmpty()) {
                    System.out.println("+++++++++++");
                }
                ps1.clearParameters();
                ps1.setString(1, c.getCustomer_id());

                ps1.setString(2, c.getPersonId());
                ps1.setString(3, ((c.getSalesPersonId() == null) ? "" : c.getSalesPersonId()));
                ps1.setString(4, c.getReference());
                ps1.setString(5, c.getCustomer_name());
                ps1.setString(6, c.getCustomer_alternative_name());
                ps1.setInt(7, c.getCustomerType());
                ps1.setInt(8, c.getInactive());
                ps1.setString(9, c.getRemarks());
                ps1.setFloat(10, c.getTradeDiscount());
                ps1.setFloat(11, c.getBalanceLimit());
                ps1.setLong(12, c.getStart_date());
                ps1.setString(13, c.getDocumentMessage());
                ps1.setString(14, c.getActivityType());
                ps1.setString(15, c.getTaxRegistrationNumber());
                ps1.setString(16, c.getTaxRegistrationOffice());
                ps1.setInt(17, c.getVatStatus());
                ps1.setString(18, c.getStore());
                ps1.setString(19, c.getAddress1());
                ps1.setString(20, c.getAddress2());
                ps1.setString(21, c.getCity());
                ps1.setString(22, c.getPostalCode());
                ps1.setString(23, c.getTelephone1());
                ps1.setString(24, c.getTelephone2());
                ps1.setString(25, c.getOther());
                ps1.setString(26, c.getSearch_field());

                ps1.setString(27, c.getPersonId());
                ps1.setString(28, ((c.getSalesPersonId() == null) ? "" : c.getSalesPersonId()));
                ps1.setString(29, c.getReference());
                ps1.setString(30, c.getCustomer_name());
                ps1.setString(31, c.getCustomer_alternative_name());
                ps1.setInt(32, c.getCustomerType());
                ps1.setInt(33, c.getInactive());
                ps1.setString(34, c.getRemarks());
                ps1.setFloat(35, c.getTradeDiscount());
                ps1.setFloat(36, c.getBalanceLimit());
                ps1.setLong(37, c.getStart_date());
                ps1.setString(38, c.getDocumentMessage());
                ps1.setString(39, c.getActivityType());
                ps1.setString(40, c.getTaxRegistrationNumber());
                ps1.setString(41, c.getTaxRegistrationOffice());
                ps1.setInt(42, c.getVatStatus());
                ps1.setString(43, c.getStore());
                ps1.setString(44, c.getAddress1());
                ps1.setString(45, c.getAddress2());
                ps1.setString(46, c.getCity());
                ps1.setString(47, c.getPostalCode());
                ps1.setString(48, c.getTelephone1());
                ps1.setString(49, c.getTelephone2());
                ps1.setString(50, c.getOther());
                ps1.setString(51, c.getSearch_field());

                try {
                    ps1.addBatch();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println("===============> : " + x);
            ps1.executeLargeBatch();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;

    }

}
