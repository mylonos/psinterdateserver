/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mylonos.dao.impl;

import com.google.common.base.Strings;
import com.mylonos.dao.CurrentStatusDAO;
import com.mylonos.dao.RouteDAO;
import com.mylonos.mappers.UserMapper;
import com.mylonos.mappers.VehicleMapper;
import com.mylonos.psinter.domain.dto.CurrentStatusDTO;
import com.mylonos.psinter.domain.dto.CustomerVisitDTO;
import com.mylonos.psinter.domain.dto.RoutesDTO;
import com.mylonos.psinter.domain.models.CurrentPotition;
import com.mylonos.psinter.domain.models.Temperature;
import com.mylonos.psinter.domain.models.User;
import com.mylonos.psinter.domain.models.Vehicle;
import com.mylonos.utils.DateHelper;
import com.mylonos.utils.Utils;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * @author manolis
 */
@Component
public class CurrentStatusDAOImpl implements CurrentStatusDAO {

    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private Utils utils;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private RouteDAO routeDAO;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        utils = new Utils();
    }

    public void setTransactionManager(PlatformTransactionManager txManager) {
        this.transactionManager = txManager;
    }

    @Override
    public void importCurrentStatus(CurrentStatusDTO csdto) {

        RoutesDTO routesDTO = csdto.getRoutes();

        String vehicleId = getVehicleId(csdto.getDevice_id());
        String userId = (csdto.getUser_id() == null || csdto.getUser_id().isEmpty()) ? getUserId(csdto.getDevice_id()) : csdto.getUser_id();
        if (Strings.isNullOrEmpty(csdto.getUser_id())) {
            csdto.setUser_id(userId);
        }

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        try {

            //migration()
//            instertCustomerVisit(csdto,vehicleId);
            String insrt_current_status = "INSERT INTO `customer_visit` "
                    + " (`visit_id`, `customer_reference`, `sales_person_id`, "
                    + " `visit_start_date`, `visit_finish_date`, `duration`, "
                    + " `timestamp`,`device_id`) VALUES (?, ?, ?, ?, ?, ?, ?,?);";

            String deleteCstm = "delete from customer_visit where timestamp>? and timestamp<? and device_id=?";

            DateHelper dateHelper = new DateHelper();
            int[] types_del = {Types.BIGINT, Types.BIGINT, Types.VARCHAR};

            jdbcTemplateObject.update(deleteCstm,
                    new Object[]{dateHelper.getTodayInMillis(),
                        dateHelper.getTomorrowInMillis(),
                        csdto.getDevice_id()
                    }, types_del);

            int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.BIGINT,
                Types.BIGINT, Types.INTEGER, Types.BIGINT, Types.VARCHAR};

            long now = System.currentTimeMillis();

            List<CustomerVisitDTO> customerVisitDTOs = csdto.getCustomerVisitDTO();
            for (CustomerVisitDTO customerVisitDTO : customerVisitDTOs) {
                jdbcTemplateObject.update(insrt_current_status,
                        new Object[]{utils.getUniqueId(), customerVisitDTO.getReference(),
                            csdto.getUser_id(), customerVisitDTO.getVisit_start_date(),
                            customerVisitDTO.getVisit_finish_date(),
                            customerVisitDTO.getDuration(),
                            now,
                            csdto.getDevice_id()
                        }, types);

            }

            //migration()
            instertCustomerVisit(csdto, vehicleId);
            instertTemperatures(csdto, vehicleId);
            routeDAO.insertRoutes(routesDTO, vehicleId);

            transactionManager.commit(txStatus);
        } catch (Exception e) {
            transactionManager.rollback(txStatus);

        }

    }

    @Override
    public void setUserPosition(CurrentPotition currentPotition) {
        String insert_last_position = "INSERT INTO `user_current_position` "
                + " (`user_id`, `device_id`, `timestamp`, `latitude`, `longitude`) VALUES (?, ?, ?, ?, ?);";

        String deleteCstm = "delete from user_current_position where user_id=? AND device_id=?";

        DateHelper dateHelper = new DateHelper();
        int[] types_del = {Types.VARCHAR, Types.VARCHAR};

        jdbcTemplateObject.update(deleteCstm,
                new Object[]{currentPotition.getUser_id(),
                    currentPotition.getDevice_id()
                }, types_del);

        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.BIGINT, Types.DOUBLE, Types.DOUBLE};
        jdbcTemplateObject.update(insert_last_position, new Object[]{currentPotition.getUser_id(),
            currentPotition.getDevice_id(), currentPotition.getTimestamp(),
            currentPotition.getLatitude(), currentPotition.getLongitude()}, types);

    }

    private void instertCustomerVisit(CurrentStatusDTO csdto, String vehicleId) {

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        try {
            String insrt_current_status = "INSERT INTO `customer_visit_migration1`\n"
                    + "(`visit_id`,\n"
                    + "`customer_reference`,\n"
                    + "`sales_person_id`,\n"
                    + "`visit_start_date`,\n"
                    + "`visit_finish_date`,\n"
                    + "`duration`,\n"
                    + "`timestamp`,\n"
                    + "`device_id`)\n"
                    + "VALUES\n"
                    + "(?,?,?,?,?,?,?,?) "
                    + "  ON DUPLICATE KEY UPDATE  "
                    + " customer_reference=? , "
                    + " sales_person_id=?,"
                    + " visit_start_date=?, "
                    + " visit_finish_date=?, "
                    + " duration=?, "
                    + " timestamp=?, "
                    + " device_id=? "
                    + "";

            DateHelper dateHelper = new DateHelper();

            int[] types = {
                Types.VARCHAR,
                Types.VARCHAR,
                Types.VARCHAR,
                Types.TIMESTAMP,
                Types.TIMESTAMP,
                Types.INTEGER,
                Types.TIMESTAMP,
                Types.VARCHAR,
                //update
                Types.VARCHAR,
                Types.VARCHAR,
                Types.TIMESTAMP,
                Types.TIMESTAMP,
                Types.INTEGER,
                Types.TIMESTAMP,
                Types.VARCHAR
            };

            long now = System.currentTimeMillis();

            List<CustomerVisitDTO> customerVisitDTOs = csdto.getCustomerVisitDTO();
            for (CustomerVisitDTO customerVisitDTO : customerVisitDTOs) {
                jdbcTemplateObject.update(insrt_current_status,
                        new Object[]{customerVisitDTO.getVisitId(),
                            customerVisitDTO.getReference(),
                            csdto.getUser_id(),
                            new Timestamp(customerVisitDTO.getVisit_start_date()),
                            new Timestamp(customerVisitDTO.getVisit_finish_date()),
                            customerVisitDTO.getDuration(),
                            new Timestamp(now),
                            csdto.getDevice_id(),
                            //update
                            customerVisitDTO.getReference(),
                            csdto.getUser_id(),
                            new Timestamp(customerVisitDTO.getVisit_start_date()),
                            new Timestamp(customerVisitDTO.getVisit_finish_date()),
                            customerVisitDTO.getDuration(),
                            new Timestamp(now),
                            csdto.getDevice_id()
                        }, types);

            }

//            routeDAO.insertRoutes(routesDTO);
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            transactionManager.rollback(txStatus);

        }

    }

    private String getVehicleId(String deviceId) {

        String getVehicleByDeviceId = "SELECT \n"
                + "    *\n"
                + "FROM\n"
                + "    vehicle\n"
                + "WHERE\n"
                + "    vehicle.vehicle_id = (SELECT \n"
                + "            employee_vehicle.vehicle_id\n"
                + "        FROM\n"
                + "            employee_vehicle\n"
                + "        WHERE\n"
                + "            employee_vehicle.employee_id = (SELECT \n"
                + "                    employee_id\n"
                + "                FROM\n"
                + "                    employee_device\n"
                + "                WHERE\n"
                + "                    device_id = ?))";

        Vehicle vehicle = jdbcTemplateObject.queryForObject(getVehicleByDeviceId, new Object[]{deviceId}, new VehicleMapper());

        return vehicle != null ? vehicle.getVehicleId() : null;
    }

    private String getUserId(String deviceId) {
        String getUserByDeviceId = "SELECT \n"
                + "    *\n"
                + "FROM\n"
                + "    user\n"
                + "WHERE\n"
                + "    user.user_id = (SELECT \n"
                + "            employee_id\n"
                + "        FROM\n"
                + "            employee_device\n"
                + "        WHERE\n"
                + "            device_id =?)";

        User user = jdbcTemplateObject.queryForObject(getUserByDeviceId, new Object[]{deviceId}, new UserMapper());

        return user != null ? user.getUserId() : null;

    }

    private void instertTemperatures(CurrentStatusDTO csdto, String vehicleId) {

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        try {
            String insertTemperature = "INSERT INTO `temperature_humidity`\n"
                    + "(`device_id`,\n"
                    + "`date_time`,\n"
                    + "`internal_temperature`,\n"
                    + "`external_temperature`,\n"
                    + "`internal_humidity`,\n"
                    + "`external_humidity`,\n"
                    + "`vehicle_id`)\n"
                    + "VALUES\n"
                    + "(?,?,?,?,?,?,?) "
                    + "  ON DUPLICATE KEY UPDATE  "
                    + " internal_temperature=? , "
                    + " external_temperature=?,"
                    + " internal_humidity=?, "
                    + " external_humidity=?, "
                    + " vehicle_id=? "
                    + "";

            int[] types = {
                Types.VARCHAR,
                Types.TIMESTAMP,
                Types.FLOAT,
                Types.FLOAT,
                Types.FLOAT,
                Types.FLOAT,
                Types.VARCHAR,
                //update
                Types.FLOAT,
                Types.FLOAT,
                Types.FLOAT,
                Types.FLOAT,
                Types.VARCHAR
            };
            List<Temperature> temperatures = csdto.getTemperatures();
            for (Temperature temperature : temperatures) {
                jdbcTemplateObject.update(insertTemperature,
                        new Object[]{
                            temperature.getDeviceId(),
                            new Timestamp(temperature.getTimestamp()),
                            temperature.getInternalTemperature(),
                            temperature.getExternalTemperature(),
                            temperature.getInternalHumidity(),
                            temperature.getExternalHumidity(),
                            vehicleId,
                            //update
                            temperature.getInternalTemperature(),
                            temperature.getExternalTemperature(),
                            temperature.getInternalHumidity(),
                            temperature.getExternalHumidity(),
                            vehicleId
                        }, types);

            }
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            transactionManager.rollback(txStatus);

        }

    }

}
