package com.mylonos.dao.impl;

import com.google.common.base.Strings;
import com.mylonos.dao.ScheduleDAO;
import com.mylonos.mappers.*;
import com.mylonos.psinter.domain.dto.ScheduleDTO;
import com.mylonos.psinter.domain.dto.ScheduleDesktopDTO;
import com.mylonos.psinter.domain.models.SchedulePoint;
import com.mylonos.psinter.domain.models.SchedulePointDetails;
import com.mylonos.utils.PasswordHash;
import com.mylonos.utils.Queries;
import com.mylonos.utils.Utils;
import java.sql.Types;
import java.util.List;
import java.util.UUID;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class ScheduleDAOImpl implements ScheduleDAO {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private PasswordHash passwordHash;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        passwordHash = new PasswordHash();
    }

    @Override
    public List<ScheduleDTO> getSchedules(String s) {
        List<ScheduleDTO> scheduleDTOs = null;

        if (Strings.isNullOrEmpty(s)) {
            scheduleDTOs = jdbcTemplateObject.query(Queries.GET_SCHEDULE, new ScheduleDTOMapper());
        } else {
            scheduleDTOs = jdbcTemplateObject.query(Queries.GET_SCHEDULE_BY_STORE, new Object[]{s}, new ScheduleDTOMapper());
        }
        for (ScheduleDTO scheduleDTO : scheduleDTOs) {
            List<SchedulePoint> schedulePoints
                    = jdbcTemplateObject.query(Queries.GET_SCHEDULE_POINTS_BY_SCHEDULE, new Object[]{scheduleDTO.getSchedule_id()}, new SchedulePointMapper());

            scheduleDTO.setSchedule_points(schedulePoints);
        }

        return scheduleDTOs;
    }

    @Override
    public List<ScheduleDesktopDTO> getSchedulesWithDetails(String s) {
//        List<ScheduleDesktopDTO> scheduleDTOs = jdbcTemplateObject.query(Queries.GET_SCHEDULE, new ScheduleDesktopDTOMapper());

        List<ScheduleDesktopDTO> scheduleDTOs = null;

        if (Strings.isNullOrEmpty(s)) {
            scheduleDTOs = jdbcTemplateObject.query(Queries.GET_SCHEDULE, new ScheduleDesktopDTOMapper());
        } else {
            scheduleDTOs = jdbcTemplateObject.query(Queries.GET_SCHEDULE_BY_STORE, new Object[]{s}, new ScheduleDesktopDTOMapper());
        }

        for (ScheduleDesktopDTO scheduleDTO : scheduleDTOs) {
            List<SchedulePointDetails> schedulePoints
                    = jdbcTemplateObject.query(Queries.GET_SCHEDULE_POINTS_BY_SCHEDULE_WITH_DETAILS, new Object[]{scheduleDTO.getSchedule_id()}, new SchedulePointDetailsMapper());

            scheduleDTO.setSchedule_points(schedulePoints);
        }
        return scheduleDTOs;
    }

    @Override
    public ScheduleDesktopDTO createSchedule(String title, String store, String user_id) {
        if (title == null || title.isEmpty()) {
            return null;
        }

        String insert = "INSERT INTO `schedule` "
                + "(`schedule_id`, "
                + "`description`, "
                + "`store`, "
                + "`user_id`) "
                + "VALUES "
                + "(?,?,?,?);";

        String uuid = UUID.randomUUID().toString();
        int[] types_insert = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};

        jdbcTemplateObject.update(insert,
                new Object[]{uuid, title, store, user_id}, types_insert);

        return new ScheduleDesktopDTO(uuid, title, store, user_id);
    }

    @Override
    public void addCustomerToSchedule(String schedule_id, String customer_id, int position) {
        if (schedule_id == null || schedule_id.isEmpty()) {
            return;
        }

        String insert = "INSERT INTO schedule_point"
                + " (`schedule_id`, "
                + " `customer_id`, "
                + " `position`) "
                + " VALUES "
                + " (?,"
                + " ?,"
                + "?);";

        String uuid = UUID.randomUUID().toString();
        int[] types_insert = {Types.VARCHAR, Types.VARCHAR, Types.INTEGER};

        jdbcTemplateObject.update(insert,
                new Object[]{schedule_id, customer_id, position}, types_insert);

    }

    @Override
    public void deleteCustomerToSchedule(String schedule_id, String customer_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
