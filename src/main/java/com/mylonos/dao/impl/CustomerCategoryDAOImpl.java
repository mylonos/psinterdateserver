package com.mylonos.dao.impl;

import com.mylonos.dao.CustomerCategoryDAO;
import com.mylonos.psinter.domain.dto.CustomerCategoryDTO;
import com.mylonos.mappers.CustomerCategoryMapper;
import com.mylonos.psinter.domain.models.CustomerCategory;
import com.mylonos.utils.PasswordHash;
import com.mylonos.utils.Queries;
import com.mylonos.utils.Utils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class CustomerCategoryDAOImpl implements CustomerCategoryDAO {

    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        utils = new Utils();
    }

    @Override
    public CustomerCategoryDTO getAllCustomerCategories() {
        List<CustomerCategory> customerCategories = new ArrayList<CustomerCategory>();
        try {
            customerCategories = jdbcTemplateObject.query(Queries.GET_CUSTOMER_CATEGORIES, new CustomerCategoryMapper());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new CustomerCategoryDTO(customerCategories);
    }

    public void addCustomerCategory(String categoryName, String parentCategoryName) {
        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
        jdbcTemplateObject.update(Queries.INSERT_CUSTOMER_CATEGORY, new Object[]{utils.getUniqueId(), categoryName, parentCategoryName}, types);

    }

    public void updateCustomerCategory(String categoryId, String categoryName) {
        int[] types = {Types.VARCHAR, Types.VARCHAR};

        jdbcTemplateObject.update(Queries.UPDATE_CUSTOMER_TYPE, new Object[]{categoryId, categoryName}, types);

    }


}
