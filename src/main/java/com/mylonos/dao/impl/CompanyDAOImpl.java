package com.mylonos.dao.impl;

import com.mylonos.psinter.domain.models.Company;
import com.mylonos.dao.CompanyDAO;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.mappers.*;
import com.mylonos.utils.Queries;
import com.mylonos.utils.Utils;
import java.sql.Types;
import java.util.*;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Component
public class CompanyDAOImpl implements CompanyDAO {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private Utils utils;
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);

    }

    @Override
    public Results getAllCompanies(String s, Integer page, Integer limit) {
        Results results = new Results();

        List<Company> companies = jdbcTemplateObject.query(Queries.GET_ALL_COMPANIES_QUERY, new Object[]{page, limit}, new CompanyMapper());
        results.setResults(companies);
        results.setTotal(companies.size());

        return results;
    }

    @Override
    public void updateCompany(Company company) {

        int[] types_update = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
            Types.VARCHAR, Types.VARCHAR};

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        try {
            jdbcTemplateObject.update(Queries.UPDATE_COMPANIES_QUERY,
                    new Object[]{company.getCompanyName(),
                        company.getCompanyVarId(),
                        company.getParrentCompanyId(),
                        company.getTelephone(),
                        company.getFax(),
                        company.getEmail(),
                        company.getAddress(),
                        company.getCompanyId()},
                    types_update);

            transactionManager.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            transactionManager.rollback(txStatus);
        }
    }

}
