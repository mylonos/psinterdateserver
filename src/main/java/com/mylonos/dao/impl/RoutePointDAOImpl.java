package com.mylonos.dao.impl;

import com.mylonos.dao.RouteDAO;
import com.mylonos.dao.RoutePointDAO;
import com.mylonos.mappers.UserMapper;
import com.mylonos.psinter.domain.models.RoutePoint;
import com.mylonos.psinter.domain.models.User;
import com.mylonos.utils.PasswordHash;
import com.mylonos.utils.Queries;
import com.mylonos.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by manolis on 12/24/2015.
 */
@Component
public class RoutePointDAOImpl implements RoutePointDAO {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private PasswordHash passwordHash;
    private Utils utils;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        passwordHash = new PasswordHash();
    }

    
}
