/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.CurrentStatusDTO;
import com.mylonos.psinter.domain.models.CurrentPotition;

/**
 *
 * @author manolis
 */
public interface CurrentStatusDAO {

    void importCurrentStatus(CurrentStatusDTO csdto);

    void setUserPosition(CurrentPotition currentPotition);

}
