package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Temperature;

/**
 * Created by manolis on 26-Mar-16.
 */
public interface TemperatureDAO {

    Temperature getLastTemperatureByVehicleId(String vehicleId);

    Results getTemperaturesForVehicle(String vehicleId, Long date, Long dateUntil);

}
