package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.ScheduleDTO;
import com.mylonos.psinter.domain.dto.ScheduleDesktopDTO;
import java.util.List;
import javax.sql.DataSource;

/**
 * Created by manolis on 12/24/2015.
 */
public interface ScheduleDAO {

    /**
     * This is the method to be used to initialize database resources ie. connection.
     */
    void setDataSource(DataSource ds);

    List<ScheduleDTO> getSchedules(String s);

    List<ScheduleDesktopDTO> getSchedulesWithDetails(String s);

    ScheduleDesktopDTO createSchedule(String title, String store, String user_id);

    void addCustomerToSchedule(String schedule_id, String customer_id, int position);

    void deleteCustomerToSchedule(String schedule_id, String customer_id);
}
