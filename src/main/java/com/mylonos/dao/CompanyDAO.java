package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Company;
import javax.sql.DataSource;

public interface CompanyDAO {

    void setDataSource(DataSource ds);

    Results getAllCompanies(String s, Integer page, Integer limit);

    void updateCompany(Company company);

}
