package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Vehicle;
import javax.sql.DataSource;

/**
 * Created by manolis on 12/24/2015.
 */
public interface VehicleDao {

    /**
     * This is the method to be used to initialize database resources ie. connection.
     */
    void setDataSource(DataSource ds);

    Results getVehicleByStore(String storeId);

    void createVehicle(Vehicle vehicle);

    void updateVehicle(Vehicle vehicle);

    void deleteVehicle(String id);

    Results getVehicleByEmployee(String employeeId);

}
