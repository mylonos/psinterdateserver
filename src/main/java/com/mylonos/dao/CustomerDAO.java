package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.CustomerDTO;
import com.mylonos.psinter.domain.dto.CustomerStatusDTO;
import com.mylonos.psinter.domain.dto.CustomerVisitDTO;
import com.mylonos.psinter.domain.dto.CustomersOnMapResults;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Customer;
import com.mylonos.psinter.domain.models.CustomerSimple;
import com.mylonos.psinter.domain.models.CustomerSimpleInvoices;
import java.util.HashMap;
import java.util.List;
import javax.sql.DataSource;

/**
 * Created by manolis on 12/24/2015.
 */
public interface CustomerDAO {

    /**
     * This is the method to be used to initialize database resources ie. connection.
     */
    void setDataSource(DataSource ds);

    CustomerDTO getAllCustomers(String s);

    List<CustomerSimple> getAllCustomersSimple(String s, int limit);

    CustomerSimple getCustomerSimple(String id);

    CustomerSimpleInvoices getCustomerSimpleInvoices(String id);

    Results getCustomerInvoices(String id, int page, int resultsPerPage);

    void updateCustomerPositions(List<Customer> customers);

    CustomersOnMapResults getAllCustomersForCompany(String company);

    List<CustomerVisitDTO> getAllCustomersForVistis(String user_id, long date, long until);

    CustomerStatusDTO getCustomerStatus(String store);

    CustomerDTO getAllCustomersByCompany(String id, String s);

    List<Customer> listAllCustomers(String s);

    List<Customer> listAllCustomersByCompany(String s);

    Customer getCustomerById(String s);

    HashMap<String, Customer> getCustomerList();

    boolean isertCustomers(CustomerDTO cdto);

}
