package com.mylonos.dao;

import com.mylonos.psinter.domain.models.MobileDatabase;
import javax.sql.DataSource;

/**
 * Created by manolis on 12/24/2015.
 */
public interface MobileDatabaseDAO {

    /**
     * This is the method to be used to initialize database resources ie. connection.
     */
    void setDataSource(DataSource ds);

    MobileDatabase getmobileDatabase();

}
