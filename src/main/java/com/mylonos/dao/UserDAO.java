package com.mylonos.dao;

import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.dto.UserCurrentPositionDTO;
import com.mylonos.psinter.domain.dto.UserDTO;
import com.mylonos.psinter.domain.models.MobileUser;
import com.mylonos.psinter.domain.models.User;
import java.util.List;
import javax.sql.DataSource;

/**
 * Created by manolis on 12/24/2015.
 */
public interface UserDAO {

    /**
     * This is the method to be used to initialize database resources ie. connection.
     */
    void setDataSource(DataSource ds);

    List<User> findByUsername(String s);

    List<MobileUser> findByUsernameMobile(String s);

    String getUsername(String username);

    List<UserCurrentPositionDTO> getUsersCurrentPosition();

    AuthenticationDTO authenticate(String username, String password);

    boolean isValidToken(String token);

    boolean deleteUser(String user);

    List<UserDTO> getUsers(String s);

    Results findByEmployeeByStore(String store, Integer isEmployee, Integer isActive);

    void createUser(User user);

    void updateUser(User user);
}
