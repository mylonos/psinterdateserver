package com.mylonos.dao;


import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import com.mylonos.psinter.domain.dto.CustomerCategoryDTO;
import com.mylonos.psinter.domain.models.CustomerCategory;
import com.mylonos.psinter.domain.models.User;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by manolis on 12/24/2015.
 */
public interface CustomerCategoryDAO {


    /**
     * This is the method to be used to initialize
     * database resources ie. connection.
     */
    void setDataSource(DataSource ds);

    CustomerCategoryDTO getAllCustomerCategories();


}
