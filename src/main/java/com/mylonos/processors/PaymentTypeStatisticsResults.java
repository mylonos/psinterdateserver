package com.mylonos.processors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylonos.psinter.domain.dto.statistics.CustomerPaymentTypeStatisticsDTO;
import com.mylonos.psinter.domain.dto.statistics.PaymentStatisticsDTO;
import com.mylonos.psinter.domain.models.Customer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by manolis on 24-Apr-16.
 */
public class PaymentTypeStatisticsResults {
//    @Autowired
//    private CustomerDAO customerDAO;

    public List<PaymentStatisticsDTO> getResults(String results, HashMap<String, Customer> customers) {

//        customerDAO.getAllCustomers("");
        List<PaymentStatisticsDTO> paymentTypeStatisticsDTOs = new ArrayList<PaymentStatisticsDTO>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode node = objectMapper.readValue(results, JsonNode.class);

            JsonNode aggregations = node.get("aggregations");
            JsonNode period = aggregations.get("period");
            JsonNode periodBuckets = period.get("buckets"); //array

            if (periodBuckets.isArray()) {
                for (final JsonNode objNode : periodBuckets) {
                    PaymentStatisticsDTO paymentStatisticsDTO = new PaymentStatisticsDTO();

                    paymentStatisticsDTO.setPeriod(objNode.get("key_as_string").asText());
                    paymentStatisticsDTO.setTotal((float) objNode.get("total").get("value").asDouble());

                    List<CustomerPaymentTypeStatisticsDTO> customerPaymentTypeStatisticsDTOs = new ArrayList<CustomerPaymentTypeStatisticsDTO>();

                    JsonNode accounts = objNode.get("account").get("buckets");
                    if (accounts.isArray()) {

                        for (final JsonNode account : accounts) {

                            CustomerPaymentTypeStatisticsDTO customerPaymentTypeStatisticsDTO = new CustomerPaymentTypeStatisticsDTO();
                            //title
//                            System.out.println(account.get("key").asText());

                            String id = account.get("key").asText();
                            Customer customer = customers.get(id);
                            customerPaymentTypeStatisticsDTO.setCustomer_id(id);
                            customerPaymentTypeStatisticsDTO.setCustomer_name(customer.getCustomer_name());
                            customerPaymentTypeStatisticsDTO.setReference(customer.getReference());
                            customerPaymentTypeStatisticsDTO.setInv(account.get("doc_count").asInt());
                            customerPaymentTypeStatisticsDTO.setTotal((float) account.get("payment").get("value").asDouble());

                            customerPaymentTypeStatisticsDTOs.add(customerPaymentTypeStatisticsDTO);
                        }
                    }

                    paymentStatisticsDTO.setProductsStatistics(customerPaymentTypeStatisticsDTOs);
                    paymentTypeStatisticsDTOs.add(paymentStatisticsDTO);
                }
            }

            return paymentTypeStatisticsDTOs;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return paymentTypeStatisticsDTOs;

    }

    public List<Customer> getCustomerResults(String results, HashMap<String, Customer> customers) {

//        customerDAO.getAllCustomers("");
        List<Customer> customers1 = new ArrayList<Customer>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode node = objectMapper.readValue(results, JsonNode.class);

            JsonNode hits = node.get("hits");
            JsonNode inner_hits = hits.get("hits");

            if (inner_hits.isArray()) {
                for (final JsonNode objNode : inner_hits) {
//                    String id = ;
                    Customer customer = customers.get(objNode.get("_id").asText());
                    if (objNode.get("sort").isArray()) {
                        for (JsonNode distance : objNode.get("sort")) {
//                            System.out.println(distance.asDouble());
                            customer.setDistance((float) distance.asDouble());
                        }
                    }

                    customers1.add(customer);
                    System.out.println();

                }
            }

            return customers1;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return customers1;

    }
}
