package com.mylonos.processors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylonos.psinter.domain.dto.statistics.ProductStatisticsDTO;
import com.mylonos.psinter.domain.dto.statistics.ProductsStatistics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manolis on 24-Apr-16.
 */
public class ProductStatisticsResults {

    public List<ProductStatisticsDTO> getResults(String results) {

        List<ProductStatisticsDTO> productStatisticsDTOs = new ArrayList<ProductStatisticsDTO>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode node = objectMapper.readValue(results, JsonNode.class);

            JsonNode aggregations = node.get("aggregations");
            JsonNode period = aggregations.get("period");
            JsonNode periodBuckets = period.get("buckets"); //array

            if (periodBuckets.isArray()) {
                for (final JsonNode objNode : periodBuckets) {
//                    System.out.println(objNode.get("key_as_string").asText());
//                    System.out.println(objNode.get("total").get("value").asDouble());
                    ProductStatisticsDTO productStatisticsDTO = new ProductStatisticsDTO();
                    productStatisticsDTO.setPeriod(objNode.get("key_as_string").asText());
                    productStatisticsDTO.setTotal((float) objNode.get("total").get("value").asDouble());
                    List<ProductsStatistics> productsStatisticses = new ArrayList<ProductsStatistics>();

                    JsonNode products = objNode.get("products").get("buckets");
                    if (products.isArray()) {

                        for (final JsonNode product : products) {

                            //title
                            System.out.println(product.get("key").asText());

                            //for each uof create a product stat
                            JsonNode uof = product.get("uof").get("buckets");
                            for (final JsonNode units : uof) {

                                productsStatisticses.add(new ProductsStatistics(product.get("key").asText(),
                                        units.get("key").asText(),
                                        (float) units.get("quantity").get("value").asDouble(),
                                        (float) units.get("total").get("value").asDouble()));

//                                System.out.println(units.get("key").asText());
//                                System.out.println(units.get("total").get("value").asDouble());
//                                System.out.println(units.get("quantity").get("value").asDouble());
                            }

                        }
                    }

                    productStatisticsDTO.setProductsStatistics(productsStatisticses);
                    productStatisticsDTOs.add(productStatisticsDTO);
                }
            }

            return productStatisticsDTOs;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return productStatisticsDTOs;

    }

}
