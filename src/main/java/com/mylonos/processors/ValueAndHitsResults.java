package com.mylonos.processors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylonos.psinter.domain.dto.statistics.ValueAndHitsDTO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by manolis on 24-Apr-16.
 */
public class ValueAndHitsResults {

    private HashMap<Integer, ArrayList<String>> colors = new HashMap<>();

    public ValueAndHitsDTO getValueAndHitsDTO(String results) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode node = objectMapper.readValue(results, JsonNode.class);
            JsonNode hits = node.get("hits");
            JsonNode aggregations = node.get("aggregations");
            JsonNode total = aggregations.get("totalValue");
            JsonNode cost = aggregations.get("costValue");
            JsonNode vat = aggregations.get("vatValue");
            JsonNode net = aggregations.get("netValue");

            return new ValueAndHitsDTO(hits.get("total").asInt(), (float) total.get("value").asDouble(),
                    (float) cost.get("value").asDouble(), (float) vat.get("value").asDouble(), (float) net.get("value").asDouble());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ValueAndHitsDTO();
    }

}
