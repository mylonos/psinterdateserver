package com.mylonos.processors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylonos.psinter.domain.dto.statistics.LineDataDTO;
import com.mylonos.psinter.domain.models.statistics.LineData;
import com.mylonos.psinter.domain.dto.highchart.LineDataHC;
import com.mylonos.psinter.domain.dto.highchart.LineDataHCDto;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by manolis on 24-Apr-16.
 */
public class LineDataResultsHightChart {

    private HashMap<Integer, ArrayList<String>> colors = new HashMap<>();

    public LineDataResultsHightChart() {

        ArrayList<String> color = new ArrayList<>();
        color.add("rgba(218,165,32,0.2)");
        color.add("rgba(218,165,32,1)");
        color.add("rgba(218,165,32,1)");
        color.add("#fff");
        color.add("#fff");
        color.add("rgba(218,165,32,1)");

        colors.put(1, color);

        color = new ArrayList<>();
        color.add("rgba(151,187,205,0.2)");
        color.add("rgba(151,187,205,1)");
        color.add("rgba(151,187,205,1)");
        color.add("#fff");
        color.add("#fff");
        color.add("rgba(151,187,205,1)");

        colors.put(2, color);

        color = new ArrayList<>();
        color.add("rgba(102, 153, 0,0.2)");
        color.add("rgba(102, 153, 0,1)");
        color.add("rgba(102, 153, 0,1)");
        color.add("#fff");
        color.add("#fff");
        color.add("rgba(102, 153, 0,1)");

        colors.put(3, color);
        color = new ArrayList<>();
        color.add("rgba(165,42,42,0.2)");
        color.add("rgba(165,42,42,1)");
        color.add("rgba(165,42,42,1)");
        color.add("#fff");
        color.add("#fff");
        color.add("rgba(165,42,42,1)");

        colors.put(4, color);
        color = new ArrayList<>();
        color.add("rgba(255,140,0,0.2)");
        color.add("rgba(255,140,0,1)");
        color.add("rgba(255,140,0,1)");
        color.add("#fff");
        color.add("#fff");
        color.add("rgba(255,140,0,1)");

        colors.put(5, color);

        color = new ArrayList<>();
        color.add("rgba(220,220,220,0.2)");
        color.add("rgba(220,220,220,1)");
        color.add("rgba(220,220,220,1)");
        color.add("#fff");
        color.add("#fff");
        color.add("rgba(220,220,220,1)");

        colors.put(6, color);

    }

    public LineDataHCDto createLineDataDTO(ArrayList<Long> labels1, String[] results) {

        LineDataHCDto lineDataDTO = new LineDataHCDto();
        List<LineDataHC> datasets = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        HashMap m = new HashMap();
        m.forEach((k, v) -> {
            System.out.println("Item : " + k + " Count : " + v);
            if ("E".equals(k)) {
                System.out.println("Hello E");
            }
        });

        for (int i = 0; i < results.length; i++) {
            ObjectMapper objectMapper = new ObjectMapper();
            final LineDataHC lineData = new LineDataHC();
            ArrayList<Float> data = new ArrayList<>();
            try {

                JsonNode node = objectMapper.readValue(results[i], JsonNode.class);
                JsonNode aggregations = node.get("aggregations");
                if (aggregations == null) {
                    continue;
                }
                JsonNode period = aggregations.get("period");

                List<JsonNode> buckets = period.findValues("buckets");
                buckets.forEach(bucket -> {
                    bucket.forEach(record -> {
                        DateFormat df = new SimpleDateFormat("MM");
                        DateFormat dfy = new SimpleDateFormat("yyyy");
                        DateFormat dp = new SimpleDateFormat("yyyy-MM-dd");
                        try {

                            labels.add(df.format(dp.parse(record.get("key_as_string").asText())));
                            lineData.setName(dfy.format(dp.parse(record.get("key_as_string").asText())));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        data.add(Float.parseFloat(record.findValue("total").findValue("value").toString()));
                    });

                });

                lineData.setData(data);
                datasets.add(lineData);

            } catch (IOException e) {
            }

        }
        lineDataDTO.setLabels(labels);
        lineDataDTO.setSeries(datasets);
        return lineDataDTO;
    }

    public LineDataDTO createSalesPerCompanyLineDataDTO(String results) {

        LineDataDTO lineDataDTO = new LineDataDTO();
        List<LineData> datasets = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        ObjectMapper objectMapper = new ObjectMapper();

        final String[] label = new String[1];
        ArrayList<Float> data = new ArrayList<>();
        HashMap<String, ArrayList<Float>> points = new HashMap<String, ArrayList<Float>>();
        try {

            JsonNode node = objectMapper.readValue(results, JsonNode.class);
            JsonNode aggregations = node.get("aggregations");
            if (aggregations == null) {
                return lineDataDTO;
            }
            JsonNode period = aggregations.get("period");
            List<JsonNode> buckets = period.findValues("buckets");
            buckets.forEach(bucket -> {

                bucket.forEach(record -> {
                    DateFormat df = new SimpleDateFormat("dd-MM");
                    DateFormat dfy = new SimpleDateFormat("yyyy");
                    DateFormat dp = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        label[0] = dfy.format(dp.parse(record.get("key_as_string").asText()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (labels.size() < bucket.size()) {
                        try {
                            labels.add(df.format(dp.parse(record.get("key_as_string").asText())));
                            label[0] = dfy.format(dp.parse(record.get("key_as_string").asText()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    JsonNode companies = record.get("code").get("buckets");
                    companies.forEach((JsonNode company) -> {
                        String companyTitle = company.get("key").asText();
                        ArrayList<Float> poindft = points.get(companyTitle);
                        if (poindft == null || poindft.isEmpty()) {
                            ArrayList<Float> value = new ArrayList<>();
                            value.add(Float.valueOf(company.get("sales").get("value").asDouble() + ""));
                            points.put(companyTitle, value);
                        } else {
                            if (points.containsKey(companyTitle)) {
                                poindft.add(Float.valueOf(company.get("sales").get("value").asDouble() + ""));
                            }
                        }

                        System.out.println(company.get("key").asText());
                        System.out.println(company.get("sales").get("value").asDouble());
                    });
                });

            });

            System.out.println(label[0]);
            int x[] = new int[]{0};
            x[0] = 0;
            points.forEach((key, value) -> {

                LineData lineData = new LineData();
                ArrayList<String> color = this.colors.get(x[0] + 1);
                x[0] = x[0] + 1;
                lineData.setFillColor(color.get(0));
                lineData.setStrokeColor(color.get(1));
                lineData.setLabel(key);
                lineData.setPointColor(color.get(2));
                lineData.setPointStrokeColor(color.get(3));
                lineData.setPointHighlightFill(color.get(4));
                lineData.setPointHighlightStroke(color.get(5));
                lineData.setData(value);

                datasets.add(lineData);
            });

//                System.out.println(labels);
        } catch (IOException e) {
            System.out.println("no data to return");
//                e.printStackTrace();
        }

        lineDataDTO.setLabels((String[]) labels.toArray(new String[]{}));
        lineDataDTO.setDatasets(datasets);
        return lineDataDTO;
    }

//
//    public static void main(String[] args) {
//        String url = "http://localhost:9200/psinterdateinvoices/_search";
//        String query = "{\n" +
//                "  \"query\": {\n" +
//                "    \"bool\": {\n" +
//                "      \"must\": [\n" +
//                "        \n" +
//                "          {\n" +
//                "\"term\": {\n" +
//                "\"cancel\": \"0\"\n" +
//                "}\n" +
//                "}\n" +
//                ",\n" +
//                "{\n" +
//                "\"term\": {\n" +
//                "\"documentTypeCode\": \"τδα\"\n" +
//                "}\n" +
//                "\n" +
//                "        },\n" +
//                "        {\n" +
//                "          \"range\": {\n" +
//                "            \"createdDate\": {\n" +
//                "              \"format\": \"yyyy-MM-dd\",\n" +
//                "              \"from\": \"2014-01-01\",\n" +
//                "              \"to\": \"2015-01-01\"\n" +
//                "            }\n" +
//                "          }\n" +
//                "        }\n" +
//                "      ]\n" +
//                "    }\n" +
//                "  },\n" +
//                "  \"size\": 0,\n" +
//                "  \"aggs\": {\n" +
//                "    \"period\": {\n" +
//                "      \"date_histogram\": {\n" +
//                "        \"field\": \"createdDate\",\n" +
//                "        \"interval\": \"1W\",\n" +
//                "        \"format\": \"yyyy-MM-dd\",\n" +
//                "        \"min_doc_count\": 0\n" +
//                "      },\n" +
//                "      \"aggs\": {\n" +
//                "        \"total\": {\n" +
//                "          \"sum\": {\n" +
//                "            \"field\": \"payableAmount\"\n" +
//                "          }\n" +
//                "        }\n" +
//                "      }\n" +
//                "    }\n" +
//                "  }\n" +
//                "}";
//
//
//        final MediaType JSON
//                = MediaType.parse("application/json; charset=utf-8");
//
//        OkHttpClient client = new OkHttpClient();
//
//        okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, query);
//        Request request = new Request.Builder()
//                .url(url)
//                .post(body)
//                .build();
//
//
//        try {
//
//
//            new LineDataResults().createLineDataDTO(new String[]{client.newCall(request).execute().body().string()});
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
}
