package com.mylonos.security.filters;

/**
 * Created by manolis on 12/26/2015.
 */
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.filter.GenericFilterBean;

public class JwtFilter extends GenericFilterBean {

    @Override
    public void doFilter(final ServletRequest req,
            final ServletResponse res,
            final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");

        //lost 3 days for this three lines.
        if ("OPTIONS".equals(request.getMethod())) {
            chain.doFilter(request, response);
            return;
        }

//        final String authHeader = request.getHeader("Authorization");
//        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
//            throw new ServletException("Missing or invalid Authorization header.");
//        }
//
//        final String token = authHeader.substring(7); // The part after "Bearer "
//
//        try {
//            final Claims claims = Jwts.parser().setSigningKey("secretkey")
//                    .parseClaimsJws(token).getBody();
//            request.setAttribute("claims", claims);
//        } catch (final SignatureException e) {
//            throw new ServletException("Invalid token.");
//        }
        chain.doFilter(req, res);
    }

}
