package com.mylonos.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by manolis on 12/26/2015.
 */
@CrossOrigin
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {


    @Override
    @CrossOrigin
    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests().antMatchers("/rest/**").authenticated();
        http.csrf().disable();
    }
}