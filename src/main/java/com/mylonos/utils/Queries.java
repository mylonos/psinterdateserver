package com.mylonos.utils;

/**
 * Created by manolis on 12/24/2015.
 */
public final class Queries {

    public static final String GET_ALL_USERS_QUERY = "select * from user;";
    public static final String AUTHENTICATE_USER_QUERY = "select * from user where username=?";
    public static final String GET_USER_TOKEN_BY_TOKEN_QUERY = "SELECT token FROM user where token=?;";
    public static final String GET_USER_TOKEN_BY_USERNAME_QUERY = "select * from user where username=?";

    public static final String GET_CUSTOMER_CATEGORIES_FORM_CUSTOMER = "SELECT distinct specialty FROM psinter.customer;";
    public static final String GET_CUSTOMER_CATEGORIES = "SELECT * FROM customer_category;";

    public static final String GET_ALL_CUSTOMERS = " select customer_id,reference,customer_name from customer  ";

    public static final String GET_ALL_CUSTOMERS_QUERY_CONFIRMED = " select customer_id,reference,customer_name,"
            + "customer_alternative_name,activity_type,start_date,store,address1,city,telephone1,telephone2,"
            + "longitude,latitude,confirmation_date,search_field,email from customer where latitude>0 order by store,longitude desc; ";

    public static final String GET_ALL_CUSTOMERS_QUERY_UNCONFIRMED = " select customer_id,reference,customer_name,"
            + "customer_alternative_name,activity_type,start_date,store,address1,city,telephone1,telephone2,"
            + "longitude,latitude,confirmation_date,search_field,email from customer where (latitude=0 or latitude is null); ";

    public static final String GET_ALL_CUSTOMERS_QUERY = " select customer_id,reference,customer_name,"
            + "customer_alternative_name,activity_type,start_date,store,address1,city,telephone1,telephone2,"
            + "longitude,latitude,confirmation_date,search_field,email from customer where search_field LIKE ? order by store,longitude desc; ";

    public static final String GET_ALL_CUSTOMERS_QUERY_SIMPLE = " select customer_id,reference,customer_name,"
            + "customer_alternative_name,activity_type,start_date,store,address1,city,telephone1,telephone2,"
            + "longitude,latitude,confirmation_date,search_field,email from customer where search_field LIKE ?  order by store,longitude desc limit ?; ";

    public static final String GET_ALL_CUSTOMER_BY_ID_QUERY_SIMPLE = " select customer_id,reference,customer_name,"
            + "customer_alternative_name,activity_type,start_date,store,address1,city,telephone1,telephone2,"
            + "longitude,latitude,confirmation_date,search_field,email from customer where customer_id=? ";

    public static final String GET_ALL_CUSTOMER_AND_INVOICES_BY_ID_QUERY_SIMPLE = " select customer_id,reference,customer_name,"
            + "customer_alternative_name,activity_type,start_date,store,address1,city,telephone1,telephone2,"
            + "longitude,latitude,confirmation_date,search_field,email from customer where customer_id=? order by store,longitude desc ";

    public static final String GET_ALL_CUSTOMER_INVOICES_BY_ID_QUERY = " select data from `invoice` where account_id=? "
            + " order by created_date desc ";

    public static final String GET_ALL_CUSTOMERS_QUERY1 = "SELECT customer.customer_id, customer.customer_reference, "
            + " customer.full_name, customer.address, customer.telephone, customer.longitude, customer.latitude, "
            + " customer.company_id,customer_category.customer_category_name FROM customer,customer_category where "
            + " customer.specialty=customer_category.customer_category_id order by customer.company_id asc;";

    public static final String GET_ALL_CUSTOMERS_BY_COMPANY_QUERY = "SELECT * FROM customer where store in ? ";
    public static final String GET_ALL_CUSTOMERS_BY_COMPANY_QUERY1 = "SELECT * FROM customer where store = ? and search_field LIKE ? order by store,longitude desc; ";

    public static final String GET_ALL_CUSTOMERS_BY_COMPANY_QUERY_CONFIRMED = "SELECT * FROM customer where store = ? and latitude >0 order by store,longitude desc";

    public static final String GET_ALL_CUSTOMERS_BY_COMPANY_QUERY_UNCONFIRMED = "SELECT * FROM customer \n"
            + "		where store = ? and (latitude=0 or latitude is null);  ";

    //Insert statements
    public static final String INSERT_TOKEN_FOR_USER_QUERY = "UPDATE user SET token = ? WHERE user_id = ?;";
    public static final String INSERT_CUSTOMER_CATEGORY = "INSERT INTO  customer_category (customer_category_id,customer_category_name,customer_parent_category) VALUES (?,?,?);";
    public static final String UPDATE_CUSTOMER_TYPE = "UPDATE `customer`\n"
            + "SET\n"
            + "`specialty` = ?\n"
            + "WHERE `specialty`=?;";

    public static final String GET_MAINTENANCES_QUERY = "SELECT person.person_id AS person_id, project.project_id AS project_id, person.first_name, person.last_name, project.address, (SELECT SUM(task.final_price) FROM task WHERE task.project_id = project.project_id) AS total, (SELECT SUM(value) FROM payment WHERE payment.project_id = project.project_id) AS paid, maintenance.notes, maintenance.position FROM person, project, maintenance WHERE project.project_type = 3 AND maintenance.project_id = project.project_id AND person.person_id = project.customer_id AND maintenance.active = 1 AND (person.search_field LIKE '%%' OR project.search_field LIKE '%%') GROUP BY project.project_id ORDER BY maintenance.position ASC";
    public static final String GET_MAINTENANCE_RECORDS = "SELECT  maintenance_record.*, (SELECT   SUM(final_price)  FROM  task WHERE  task_id in (SELECT  distinct task_id  FROM maintenance_record_tasks  WHERE  maintenance_record_tasks.maintenance_record_id = maintenance_record.maintenance_record_id )) AS total  FROM maintenance_record  WHERE    maintenance_id = ? AND date_for >= ?  AND date_for <= ?  ORDER BY date_for ASC ";

    public static final String GET_MOBILE_DATABASE = " SELECT * FROM Mmobile_database order by database_id desc limit 1;";

    //    Route
    public static final String GET_ROUTE = " select * from route";
    public static final String GET_ROUTE_POINTS = " select * from route_point";
    public static final String GET_ROUTE_POINTS_BY_ROUTE = " select * from route_point where route_id=?";

    //    Schedule
    public static final String GET_SCHEDULE = " select * from schedule";
    public static final String GET_SCHEDULE_BY_STORE = " select * from schedule where store=?";
    public static final String GET_SCHEDULE_POINTS = " select * from schedule_point";
    public static final String GET_SCHEDULE_POINTS_BY_SCHEDULE = " select * from schedule_point where schedule_id=?";

    public static final String GET_SCHEDULE_POINTS_BY_SCHEDULE_WITH_DETAILS = " select customer.customer_id, "
            + " customer.reference,customer.customer_name,customer.customer_alternative_name,customer.activity_type, "
            + " customer.start_date,customer.store,customer.address1,customer.city,customer.telephone1,customer.telephone2, "
            + " schedule_point.schedule_id,schedule_point.position "
            + " from customer,schedule_point where schedule_point.schedule_id=? AND"
            + " schedule_point.customer_id = customer.customer_id order by schedule_point.position";

    public static final String GET_CUSTOMER_VISITS = " SELECT c.customer_name,c.activity_type,"
            + " c.telephone1,c.telephone2,c.latitude,c.longitude,cv.* FROM customer_visit cv, customer c "
            + " WHERE c.reference = cv.customer_reference  AND cv.sales_person_id=?  and "
            + " (cv.visit_start_date>? and cv.visit_start_date<=?) order by cv.visit_start_date asc; ";

    //Companies
    public static final String GET_ALL_COMPANIES_QUERY = "SELECT * FROM company limit ?,?;";

    public static final String UPDATE_COMPANIES_QUERY = " UPDATE 'company'\n"
            + "SET 'company_name' = ?, company_vat_id' = ?,'parent_company' = ? WHERE 'company_id' = ?;";

}
