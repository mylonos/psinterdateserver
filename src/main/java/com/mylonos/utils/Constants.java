package com.mylonos.utils;

/**
 * Created by manolis on 09-May-16.
 */
public class Constants {

    public static String ELS_END_POINT = "http://localhost:9200/_search";
    public static String ELS_END_CUSTOMERS = "http://localhost:9200/customers/_search";
    public static String ELS_END_POINT_INVOICES = "http://localhost:9200/invoices/_search";
    public static String ELS_END_POINT_PRODUCTS = "http://localhost:9200/products/_search";

}
