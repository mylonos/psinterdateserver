package com.mylonos.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by manolis on 09-Apr-16.
 */
public class DateHelper {

    public String getTodayReversed() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getToday() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public long getTodayInMillis() {
        Date date = new Date();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        Date date2 = null;
        try {
            date2 = format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date2);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTimeInMillis();
    }

    public long getDateInMillis(String inDate) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = format.parse(inDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date2 = null;
        try {
            date2 = format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date2);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTimeInMillis();
    }

    public Long getGrDateInMillis(String inDate) {

        if (null == inDate || inDate.isEmpty()) {
            return null;
        }
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(inDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date2 = null;
        try {
            date2 = format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date2);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTimeInMillis();
    }

    public long getTomorrowInMillis() {
        Date date = new Date();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date date2 = null;
        try {
            date2 = format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date2);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        return calendar.getTimeInMillis();
    }

    public Long getDateNextInMillis(String inDate) {

        if (null == inDate || inDate.isEmpty()) {
            return null;
        }
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        Date date = null;
        try {
            date = format.parse(inDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date2 = null;
        try {
            date2 = format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date2);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        return calendar.getTimeInMillis();
    }

    public Long getGrDateNextInMillis(String inDate) {

        if (null == inDate || inDate.isEmpty()) {
            return null;
        }
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date date = null;
        try {
            date = format.parse(inDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date2 = null;
        try {
            date2 = format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date2);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        return calendar.getTimeInMillis();
    }

    public String millisToTime(long millis) {
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return hms;

    }

    public String millisToDate(long millis) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyy hh:mm:ss");

        return simpleDateFormat.format(new Date(millis));

    }

}
