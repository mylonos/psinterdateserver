package com.mylonos.utils;

import java.util.UUID;

/**
 * Created by manolis on 12/24/2015.
 */
public class Utils {

    private final String salt = "6t1ocp3scqkqcccoumi609s584";

    public String getUniqueId() {
        return UUID.randomUUID().toString();
    }
}
