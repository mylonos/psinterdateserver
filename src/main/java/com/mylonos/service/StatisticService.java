/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mylonos.service;

import com.mylonos.psinter.domain.dto.highchart.LineDataHC;
import com.mylonos.utils.Utils;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author manolis
 */
@Component
public class StatisticService {

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplateObject;
    private Utils utils;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);

    }

    public LineDataHC calculateStatsForOpeningsPerMonthPerStore(String store, String from, String until) {

        LineDataHC lnhc = new LineDataHC();

        String query = ""
                + ""
                + ""
                + "SELECT \n"
                + "    c.company_name,\n"
                + "    DATE_FORMAT(stat.issue_date, '%m-%Y'),\n"
                + "    COUNT(*)\n"
                + "FROM\n"
                + "    stats_first_invoice_for_customer_of_each_year stat,\n"
                + "    company c\n"
                + "WHERE\n"
                + "        stat.store = c.company_id AND "
                + "    stat.store = ? \n"
                + "        AND stat.issue_date >= DATE(?)\n"
                + "        AND stat.issue_date <= DATE(?)\n"
                + "GROUP BY stat.store , DATE_FORMAT(stat.issue_date, '%m-%Y')\n"
                + ";"
                + "";

        PreparedStatement ps = null;

        ArrayList<String> labels = new ArrayList<>();
        ArrayList<Float> values = new ArrayList<>();
        HashMap<String, Object> extras = new HashMap();
        HashMap<Integer, String> counters = new HashMap();
        SqlRowSet rs = null;
        try {
            rs = jdbcTemplateObject.queryForRowSet(query, new Object[]{store, from, until});

            Integer ctr = 0;
            Integer total = 0;

            while (rs.next()) {

                lnhc.setName(rs.getString(1));
                labels.add(rs.getString(2));
                values.add(rs.getFloat(3));
                total += rs.getInt(3);
                counters.put(++ctr, total + "");

            }

        } // Handle any errors that may have occurred.
        catch (Exception e) {
            Logger.getLogger(StatisticService.class.getName()).log(Level.SEVERE, null, e);

        }

        lnhc.setData(values);
        extras.put("labels", labels);
        extras.put("counters", counters);
        lnhc.setExtra(extras);

        return lnhc;
    }

    public LineDataHC calculateStatsForOpeningsPerMonthPerStore1(String store, String from, String until) {

        LineDataHC lnhc = new LineDataHC();

        String query = ""
                + ""
                + ""
                + "SELECT \n"
                + "    c.company_name,\n"
                + "    DATE_FORMAT(stat.issue_date, '%m-%Y'),\n"
                + "    COUNT(*)\n"
                + "FROM\n"
                + "    stats_first_invoice_for_customer_of_each_year stat,\n"
                + "    company c\n"
                + "WHERE\n"
                + "        stat.store = c.company_id AND "
                + "    stat.store = ? \n"
                + "        AND stat.issue_date >= DATE(?)\n"
                + "        AND stat.issue_date <= DATE(?)\n"
                + "GROUP BY stat.store , DATE_FORMAT(stat.issue_date, '%m-%Y')\n"
                + ";"
                + "";

        PreparedStatement ps = null;

        ArrayList<String> labels = new ArrayList<>();
        ArrayList<Float> values = new ArrayList<>();
        HashMap<String, Object> extras = new HashMap();
        HashMap<Integer, String> counters = new HashMap();
        SqlRowSet rs = null;
        try {
            rs = jdbcTemplateObject.queryForRowSet(query, new Object[]{store, from, until});

            Integer ctr = 0;
            Integer total = 0;

            while (rs.next()) {

                lnhc.setName(rs.getString(1));
                labels.add(rs.getString(2));
                values.add(rs.getFloat(3));
                total += rs.getInt(3);
                counters.put(++ctr, total + "");

            }

        } // Handle any errors that may have occurred.
        catch (Exception e) {
            Logger.getLogger(StatisticService.class.getName()).log(Level.SEVERE, null, e);

        }
//        lnhc.setName(store);
        lnhc.setData(values);
        extras.put("labels", labels);
        extras.put("counters", counters);
        lnhc.setExtra(extras);

        return lnhc;
    }

    public LineDataHC calculateSalesStatsPerCustomer(String customer, String from, String until, String interval, String year) {

        LineDataHC lnhc = new LineDataHC();
        Logger.getLogger(StatisticService.class.getName()).log(Level.FINE, null, "");
        System.out.println("***************************************");
        System.out.println("***************************************");
        System.out.println("**** sales per week for customer ****");
        System.out.println("***************************************");
        System.out.println("***************************************");

        String query = ""
                + ""
                + "SELECT \n"
                + "    YEARWEEK(inv.deleivery_due_date) weekq,\n"
                + "    SUM(inv.payable_amount) total\n"
                + "FROM\n"
                + "    invoice inv\n"
                + "WHERE\n"
                + "    inv.account_id = ? \n"
                + "  AND  inv.cancel = 0 \n"
                + "        AND ( inv.document_type_code= 'ΤΔΑ' OR inv.document_type_code= '1003')\n"
                + "        AND inv.deleivery_due_date >= DATE(?)\n"
                + "        AND inv.deleivery_due_date <= DATE(?)\n"
                + "GROUP BY  YEARWEEK(inv.deleivery_due_date)\n"
                + "ORDER BY weekq ASC\n"
                + ";"
                + ""
                + ""
                + "";

        PreparedStatement ps = null;

        ArrayList<String> labels = new ArrayList<>();
        ArrayList<Float> values = new ArrayList<>();
        HashMap<String, Object> extras = new HashMap();
        HashMap<Integer, String> counters = new HashMap();
        lnhc.setName(year);
        SqlRowSet rs = null;
        try {
            rs = jdbcTemplateObject.queryForRowSet(query, new Object[]{customer, from, until});

            Integer ctr = 0;
            Integer total = 0;

            if ("1W".equalsIgnoreCase(interval)) {
                for (int i = 0; i < 54; i++) {
                    labels.add("" + i + 1);
                    values.add(0F);
                }
            }

            while (rs.next()) {

                if ("1W".equalsIgnoreCase(interval)) {

//                    String year = rs.getString(1).substring(0, 4);
                    lnhc.setName(year);
//                    System.out.println(rs.getFloat(2));
                    String week = rs.getString(1).substring(4);
                    values.set(Integer.parseInt(week), rs.getFloat(2));

                } else {
                    lnhc.setName("");
                    labels.add(rs.getString(1));
                    values.add(rs.getFloat(2));
                    total += rs.getInt(2);
                    counters.put(++ctr, total + "");
                }

            }

        } // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
        }
//        lnhc.setName(store);
        lnhc.setData(values);
        extras.put("labels", labels);
        extras.put("counters", counters);
        lnhc.setExtra(extras);

        return lnhc;
    }

    public LineDataHC calculateSalesStatsForPerMonthPerStore(String store, String from, String until, String interval, Long year) {

        LineDataHC lnhc = new LineDataHC();

        String query = ""
                + ""
                + ""
                + "SELECT \n"
                + "    c.company_name,\n"
                + "    DATE_FORMAT(inv.deleivery_due_date, '%m-%Y'),\n"
                + "    SUM(total_value) total\n"
                + "FROM\n"
                + "    invoice inv,\n"
                + "    company c\n"
                + "WHERE\n"
                + "    inv.company_code = ?\n"
                + "  AND  inv.cancel = 0 \n"
                + "        AND ( inv.document_type_code= 'ΤΔΑ' OR inv.document_type_code= '1003')\n"
                + "        AND c.company_id = inv.company_code\n"
                + "        AND inv.deleivery_due_date >= DATE(?)\n"
                + "        AND inv.deleivery_due_date <= DATE(?)\n"
                + " GROUP BY inv.company_code , DATE_FORMAT(inv.deleivery_due_date, '%m-%Y')\n"
                + ";"
                + ""
                + ""
                + "";

        PreparedStatement ps = null;

        ArrayList<String> labels = new ArrayList<>();
        ArrayList<Float> values = new ArrayList<>();
        HashMap<String, Object> extras = new HashMap();
        HashMap<Integer, String> counters = new HashMap();
        SqlRowSet rs = null;
        try {
            rs = jdbcTemplateObject.queryForRowSet(query, new Object[]{store, from, until});

            Integer ctr = 0;
            Float total = 0F;

            while (rs.next()) {

                lnhc.setName(year + "");
                labels.add(rs.getString(2));
                values.add(rs.getFloat(3));
                total += rs.getFloat(3);
                counters.put(++ctr, total + "");

            }

        } // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
        }
//        lnhc.setName(store);
        lnhc.setData(values);
        extras.put("labels", labels);
        extras.put("counters", counters);
        lnhc.setExtra(extras);
        return lnhc;
    }

    public LineDataHC calculateQuantitiesStatsPerWeekPerStore(String store, String from, String until, String interval, Long year, String unitOfMeasure) {

        LineDataHC lnhc = new LineDataHC();
        System.out.println("***************************************");
        System.out.println("***************************************");
        System.out.println("**** sales per month ****");
        System.out.println("***************************************");
        System.out.println("***************************************");

        String query = ""
                + ""
                + "SELECT \n"
                + "    c.company_name,\n"
                + "     YEARWEEK(inv.creation_date) weekq,\n"
                + "    SUM(inv.quantity) quantity,\n"
                + "    SUM(inv.total_value) total\n"
                + "FROM\n"
                + "    stats_invoiced_products inv,\n"
                + "    company c\n"
                + "WHERE\n"
                + "    inv.company_code = ?\n"
                + "    AND inv.cancel = 0\n"
                + "        AND (inv.other_flag = -1 OR inv.other_flag= 2)\n"
                + "        AND (inv.documentType = 'ΤΔΑ' OR inv.documentType = '1003')\n"
                + "        AND c.company_id = inv.company_code\n"
                + "        AND inv.unit_of_measure = ?\n"
                + "        AND inv.creation_date >= DATE(?)\n"
                + "        AND inv.creation_date <= DATE(?)\n"
                + "GROUP BY inv.company_code ,  YEARWEEK(inv.creation_date)\n"
                + "ORDER BY weekq asc\n"
                + ";"
                + ""
                + ""
                + ""
                + "";

        PreparedStatement ps = null;

        ArrayList<String> labels = new ArrayList<>();
        ArrayList<Float> values = new ArrayList<>();
        HashMap<String, Object> extras = new HashMap();
        HashMap<Integer, String> counters = new HashMap();
        SqlRowSet rs = null;
        try {
            rs = jdbcTemplateObject.queryForRowSet(query, new Object[]{store, unitOfMeasure, from, until});
            Integer ctr = 0;
            Float total = 0F;

            while (rs.next()) {

                lnhc.setName(rs.getString(1) + "");
                labels.add(rs.getString(2));
                values.add(rs.getFloat(3));
                total += rs.getFloat(3);
                counters.put(++ctr, total + "");

            }

        } // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
        }
//        lnhc.setName(store);
        lnhc.setData(values);
        extras.put("labels", labels);
        extras.put("counters", counters);
        lnhc.setExtra(extras);

        return lnhc;
    }

    public LineDataHC calculateSalesStatsPerWeekPerStore(String store, String from, String until, String interval, Long year, String unitOfMeasure) {

        LineDataHC lnhc = new LineDataHC();
        System.out.println("***************************************");
        System.out.println("***************************************");
        System.out.println("**** sales per month ****");
        System.out.println("***************************************");
        System.out.println("***************************************");

        String query = ""
                + ""
                + " SELECT \n"
                + "    c.company_name,\n"
                + "     YEARWEEK(inv.creation_date) weekq,\n"
                + "    SUM(inv.total_value) total\n"
                + " FROM\n"
                + "    stats_invoiced_products inv,\n"
                + "    company c\n"
                + " WHERE\n"
                + "    inv.company_code = ?\n"
                + "    AND inv.cancel = 0\n"
                + "         AND (inv.other_flag = - 1\n"
                + "        OR inv.other_flag = 2\n"
                + "        OR inv.other_flag = 1\n"
                + "        OR inv.other_flag = 0\n"
                + "        ) "
                + "        AND (inv.documentType = 'ΤΔΑ' OR inv.documentType = '1003') \n"
                + "        AND c.company_id = inv.company_code\n"
                + "        AND inv.unit_of_measure = ?\n"
                + "        AND inv.creation_date >= DATE(?)\n"
                + "        AND inv.creation_date <= DATE(?)\n"
                + " GROUP BY inv.company_code ,  YEARWEEK(inv.creation_date)\n"
                + " ORDER BY weekq asc\n"
                + ";"
                + ""
                + ""
                + ""
                + "";

        PreparedStatement ps = null;

        ArrayList<String> labels = new ArrayList<>();
        ArrayList<Float> values = new ArrayList<>();
        HashMap<String, Object> extras = new HashMap();
        HashMap<Integer, String> counters = new HashMap();

        SqlRowSet rs = null;
        try {
            rs = jdbcTemplateObject.queryForRowSet(query, new Object[]{store, unitOfMeasure, from, until});

            Integer ctr = 0;
            Float total = 0F;

            while (rs.next()) {

                lnhc.setName(rs.getString(1) + "");
                labels.add(rs.getString(2));
                values.add(rs.getFloat(3));
                total += rs.getFloat(3);
                counters.put(++ctr, total + "");

            }

        } // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
        }
//        lnhc.setName(store);
        lnhc.setData(values);
        extras.put("labels", labels);
        extras.put("counters", counters);
        lnhc.setExtra(extras);
        return lnhc;
    }

    public HashMap calculateSalesStatsPerStorePerProduct(String store, String from, String until, String interval, Long year, String unitOfMeasure, String order) {

        HashMap<String, Object> results = new HashMap<>();

        LineDataHC lnhc = new LineDataHC();
        System.out.println("***************************************");
        System.out.println("***************************************");
        System.out.println("**** calculateSalesStatsPerStorePerProduct ****");
        System.out.println("***************************************");
        System.out.println("***************************************");

        String query = ""
                + ""
                + "  SELECT \n"
                + "     c.company_name,\n"
                + "     inv.code,\n"
                + "     SUM(inv.total_value) total,\n"
                + "     SUM(inv.quantity) quantities\n"
                + " FROM\n"
                + "     stats_invoiced_products inv,\n"
                + "     company c\n"
                + " WHERE\n"
                + "     inv.company_code = ?\n"
                + "    AND inv.cancel = 0\n"
                + " AND (inv.other_flag = - 1\n"
                + "        OR inv.other_flag = 2\n"
                + "        OR inv.other_flag = 1\n"
                + "        OR inv.other_flag = 0\n"
                + "        )"
                + "        AND (inv.documentType = 'ΤΔΑ' OR inv.documentType = '1003')\n"
                + "         AND c.company_id = inv.company_code\n"
                + "         AND inv.unit_of_measure = ?\n"
                + "         AND inv.creation_date >= DATE(?)\n"
                + "         AND inv.creation_date <= DATE(?)\n"
                + " GROUP BY inv.code\n"
                + " ORDER BY total " + order + "\n"
                + " LIMIT 0 , 10\n"
                + " ;"
                + ""
                + ""
                + "";

        PreparedStatement ps = null;

        ArrayList<String> labels = new ArrayList<>();
        ArrayList<Float> values = new ArrayList<>();
        HashMap<String, Object> extras = new HashMap();
        HashMap<Integer, String> counters = new HashMap();

        SqlRowSet rs = null;
        try {
            rs = jdbcTemplateObject.queryForRowSet(query, new Object[]{store, unitOfMeasure, from, until});

            ArrayList<HashMap> data = new ArrayList();
            while (rs.next()) {

                HashMap<String, String> resultRow = new HashMap<>();
                lnhc.setName(rs.getString(1) + "");
                labels.add(rs.getString(2));
                values.add(rs.getFloat(3));
                resultRow.put("productCode", rs.getString(2));
                resultRow.put("productName", "");
                resultRow.put("totalSales", rs.getFloat(3) + "");
                resultRow.put("totalQuantity", rs.getFloat(4) + "");
                resultRow.put("unitOfMeasure", unitOfMeasure + "");
                data.add(resultRow);
            }
            results.put("results", data);

        } // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
        }
//        lnhc.setName(store);
        lnhc.setData(values);
        extras.put("labels", labels);
        extras.put("counters", counters);
        lnhc.setExtra(extras);
        return results;
    }

}
