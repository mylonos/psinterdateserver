/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mylonos.service;

import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Vehicle;

/**
 *
 * @author manolis
 */
public interface VehicleService {

    public Results getVehicles(int page, int resultsPerPage);

    public Vehicle getVehicleByID(String id);

    public Results getVehicleByName(String name);

    public void updateVehicle(Vehicle vehicle);

}
