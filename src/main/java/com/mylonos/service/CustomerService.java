/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mylonos.service;

import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Company;

/**
 *
 * @author manolis
 */
public interface CustomerService {

    public Results getCompanies(int page, int resultsPerPage);

    public Company getCompanyByID();

    public Results getCompaniesByName(String name, int page, int resultsPerPage);

    public void updateCompany(Company company);

}
