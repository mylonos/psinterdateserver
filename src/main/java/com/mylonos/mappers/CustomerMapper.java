package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.models.Customer;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CustomerMapper implements RowMapper<Customer> {

    @Override
    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Customer customer = new Customer(
                rs.getString("customer_id"),
                rs.getString("reference"),
                rs.getString("customer_name"),
                rs.getString("customer_alternative_name"),
                rs.getString("address1"),
                rs.getString("activity_type"),
                rs.getLong("start_date"),
                rs.getFloat("longitude"),
                rs.getFloat("latitude"),
                rs.getFloat("confirmation_date"),
                rs.getString("store"),
                rs.getString("city"),
                rs.getString("telephone1"),
                rs.getString("telephone2"),
                //                rs.getString("confirmation_device"),
                //                rs.getString("confirmed_by"),
                rs.getString("search_field"),
                rs.getString("email")
        );
        return customer;
    }

}
