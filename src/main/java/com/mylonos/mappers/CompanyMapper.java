package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.models.Company;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CompanyMapper implements RowMapper<Company> {

    @Override
    public Company mapRow(ResultSet rs, int rowNum) throws SQLException {
        Company company = new Company(
                rs.getString("company_id"),
                rs.getString("company_name"),
                rs.getString("company_vat_id"),
                rs.getString("parent_company"),
                rs.getString("telephone"),
                rs.getString("fax"),
                rs.getString("email"),
                rs.getString("address")
        );
        return company;
    }

}
