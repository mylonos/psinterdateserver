package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */


import com.mylonos.psinter.domain.models.SchedulePoint;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SchedulePointMapper implements RowMapper<SchedulePoint> {

    @Override
    public SchedulePoint mapRow(ResultSet rs, int rowNum) throws SQLException {
        SchedulePoint route = new SchedulePoint(
                rs.getString("schedule_id"),
                rs.getString("customer_id"),
                rs.getInt("position")
        );
        return route;
    }

}
