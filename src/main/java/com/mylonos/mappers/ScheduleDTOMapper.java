package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.dto.ScheduleDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ScheduleDTOMapper implements RowMapper<ScheduleDTO> {

    @Override
    public ScheduleDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ScheduleDTO route = new ScheduleDTO(
                rs.getString("schedule_id"),
                rs.getString("description"),
                rs.getString("store"),
                rs.getString("user_id")
        );
        return route;
    }

}
