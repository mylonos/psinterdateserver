package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.dto.CustomerVisitDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CustomerVistisDTOMapper implements RowMapper<CustomerVisitDTO> {

    @Override
    public CustomerVisitDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CustomerVisitDTO customer = new CustomerVisitDTO(
                rs.getString("customer_name"),
                rs.getString("customer_reference"),
                rs.getString("activity_type"),
                rs.getString("telephone1"),
                rs.getString("telephone2"),
                rs.getLong("visit_start_date"),
                rs.getLong("visit_finish_date"),
                rs.getInt("duration"),
                rs.getFloat("latitude"),
                rs.getFloat("longitude"),
                rs.getString("device_id"),
                rs.getString("visit_id")
        );
        return customer;
    }

}
