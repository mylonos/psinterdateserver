package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.dto.UserDTO;
import com.mylonos.psinter.domain.models.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDTOMapper implements RowMapper<UserDTO> {

    @Override
    public UserDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            UserDTO user = new UserDTO(
                rs.getString("user_id"),
                rs.getString("username"),
                rs.getString("token"),
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getInt("type"),
                rs.getString("store")
        );
        return user;
    }

}
