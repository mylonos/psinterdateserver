package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.models.RoutePoint;
import com.mylonos.psinter.domain.models.Segment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SegmentPointMapper implements RowMapper<RoutePoint> {


    @Override
    public RoutePoint mapRow(ResultSet rs, int rowNum) throws SQLException {
        RoutePoint customer = new RoutePoint(
                rs.getString("route_point_record_id"),
                rs.getDouble("longitude"),
                rs.getDouble("latitude"),
                rs.getLong("timestamp"),
                rs.getString("data"),
                rs.getString("device_id"),
                rs.getFloat("speed"),
                rs.getFloat("bearing"),
                rs.getFloat("accuracy")
        );
        return customer;
    }

}
