package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.models.Route;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author manolis
 */
public class RouteMapper implements RowMapper<Route> {

//    public Route(String , long date, String , String , String , float ) {
    @Override
    public Route mapRow(ResultSet rs, int rowNum) throws SQLException {
        Route customer = new Route(
                rs.getString("route_id"),
                rs.getLong("date"),
                rs.getString("desc"),
                rs.getString("user_id"),
                rs.getString("device_id"),
                rs.getString("vehicle_id")
        );
        return customer;
    }

}
