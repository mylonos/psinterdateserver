package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */


import com.mylonos.psinter.domain.models.SchedulePoint;
import com.mylonos.psinter.domain.models.SchedulePointDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SchedulePointDetailsMapper implements RowMapper<SchedulePointDetails> {


    @Override
    public SchedulePointDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        SchedulePointDetails route = new SchedulePointDetails(
                rs.getString("schedule_id"),
                rs.getString("customer_id"),
                rs.getInt("position"),
                rs.getString("reference"),
                rs.getString("customer_name"),
                rs.getString("customer_alternative_name"),
                rs.getString("address1"),
                rs.getString("activity_type"),
                rs.getString("telephone1"),
                rs.getString("telephone2")
                );
        return route;
    }

}
