package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.models.CustomerSimple;
import com.mylonos.psinter.domain.models.CustomerSimpleInvoices;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerSimpleInvoicesMapper implements RowMapper<CustomerSimpleInvoices> {
    @Override
    public CustomerSimpleInvoices mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerSimpleInvoices customer = new CustomerSimpleInvoices(
                rs.getString("customer_id"),
                rs.getString("reference"),
                rs.getString("customer_name"),
                rs.getString("customer_alternative_name"),
                rs.getString("address1"),
                rs.getString("activity_type"),
                rs.getFloat("start_date"),
                rs.getFloat("longitude"),
                rs.getFloat("latitude"),
                rs.getFloat("confirmation_date"),
                rs.getString("store"),
                rs.getString("city"),
                rs.getString("telephone1"),
                rs.getString("telephone2"),
                rs.getString("search_field")
        );
        return customer;
    }

}
