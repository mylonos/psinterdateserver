package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.dto.CustomerForMapDTO;
import com.mylonos.psinter.domain.models.Customer;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerForMapMapper implements RowMapper<CustomerForMapDTO> {
    @Override
    public CustomerForMapDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerForMapDTO customer = new CustomerForMapDTO(
                rs.getString("customer_id"),
                rs.getString("reference"),
                rs.getString("customer_name"),
                rs.getString("customer_alternative_name"),
                rs.getString("address1"),
                rs.getString("activity_type"),
                rs.getFloat("start_date"),
                rs.getFloat("longitude"),
                rs.getFloat("latitude"),
                rs.getFloat("confirmation_date"),
                rs.getString("store"),
                rs.getString("city"),
                rs.getString("telephone1"),
                rs.getString("telephone2")
        );
        return customer;
    }

}
