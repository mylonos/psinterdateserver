package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.models.Vehicle;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class VehicleMapper implements RowMapper<Vehicle> {

    @Override
    public Vehicle mapRow(ResultSet rs, int rowNum) throws SQLException {
        Vehicle vehicle = new Vehicle(
                rs.getString("vehicle_id"),
                rs.getString("plate"),
                rs.getDate("first_licence").getTime(),
                rs.getDate("acquisition_date").getTime(),
                rs.getInt("status"),
                rs.getString("store"),
                rs.getString("vehicle_description"),
                rs.getString("search_field")
        );
        return vehicle;
    }

}
