package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.models.Customer;
import com.mylonos.psinter.domain.models.MobileDatabase;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MobileDatabaseMapper implements RowMapper<MobileDatabase> {

    @Override
    public MobileDatabase mapRow(ResultSet rs, int rowNum) throws SQLException {
        MobileDatabase customer = new MobileDatabase(
                rs.getInt("database_id"),
                rs.getString("database_schema")
        );
        return customer;
    }

}
