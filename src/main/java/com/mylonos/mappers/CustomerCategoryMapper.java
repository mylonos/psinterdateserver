package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.models.CustomerCategory;
import com.mylonos.psinter.domain.models.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerCategoryMapper implements RowMapper<CustomerCategory> {

    @Override
    public CustomerCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerCategory customerCategory = new CustomerCategory(
                rs.getString("customer_category_id"),
                rs.getString("customer_category_name"),
                rs.getString("customer_parent_category")
        );
        return customerCategory;
    }

}
