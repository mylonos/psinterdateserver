package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.models.Route;
import com.mylonos.psinter.domain.models.Segment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SegmentMapper implements RowMapper<Segment> {

//    public Route(String , long date, String , String , String , float ) {

    @Override
    public Segment mapRow(ResultSet rs, int rowNum) throws SQLException {
        Segment customer = new Segment(
                rs.getString("segment_id"),
                rs.getFloat("total_in_meter")
        );
        return customer;
    }

}
