package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.dto.ScheduleDTO;
import com.mylonos.psinter.domain.dto.ScheduleDesktopDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ScheduleDesktopDTOMapper implements RowMapper<ScheduleDesktopDTO> {

    @Override
    public ScheduleDesktopDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ScheduleDesktopDTO route = new ScheduleDesktopDTO(
                rs.getString("schedule_id"),
                rs.getString("description"),
                rs.getString("store"),
                rs.getString("user_id")
        );
        return route;
    }

}
