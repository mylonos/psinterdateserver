package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.models.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {

        User user = new User(
                rs.getString("user_id"),
                rs.getString("username"),
                rs.getString("password"),
                rs.getString("token"),
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getString("store"),
                rs.getInt("type"),
                rs.getTimestamp("last_update").getTime(),
                rs.getInt("is_employee"),
                rs.getInt("is_active"),
                rs.getString("email")
        );
        return user;
    }

}
