package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.models.Temperature;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TemperatureMapper implements RowMapper<Temperature> {

    @Override
    public Temperature mapRow(ResultSet rs, int rowNum) throws SQLException {
        Temperature temperature = new Temperature(
                rs.getString("device_id"),
                rs.getString("vehicle_id"),
                rs.getTimestamp("date_time").getTime(),
                rs.getFloat("internal_temperature"),
                rs.getFloat("external_temperature"),
                rs.getFloat("internal_humidity"),
                rs.getFloat("external_humidity"));
        return temperature;
    }

}
