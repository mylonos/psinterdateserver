package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */

import com.mylonos.psinter.domain.dto.UserCurrentPositionDTO;
import com.mylonos.psinter.domain.models.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserCurrentPositionDTOMapper implements RowMapper<UserCurrentPositionDTO> {

    @Override
    public UserCurrentPositionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserCurrentPositionDTO user = new UserCurrentPositionDTO(
                rs.getString("user_id"),
                rs.getString("device_id"),
                rs.getLong("timestamp"),
                rs.getFloat("latitude"),
                rs.getFloat("longitude"),
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getString("store")
        );

        return user;
    }

}
