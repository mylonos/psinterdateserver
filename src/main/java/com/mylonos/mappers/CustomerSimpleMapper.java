package com.mylonos.mappers;

/**
 * Created by manolis on 12/24/2015.
 */
import com.mylonos.psinter.domain.models.CustomerSimple;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CustomerSimpleMapper implements RowMapper<CustomerSimple> {

    @Override
    public CustomerSimple mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerSimple customer = new CustomerSimple(
                rs.getString("customer_id"),
                rs.getString("reference"),
                rs.getString("customer_name"),
                rs.getString("customer_alternative_name"),
                rs.getString("address1"),
                rs.getString("activity_type"),
                rs.getFloat("start_date"),
                rs.getFloat("longitude"),
                rs.getFloat("latitude"),
                rs.getFloat("confirmation_date"),
                rs.getString("store"),
                rs.getString("city"),
                rs.getString("telephone1"),
                rs.getString("telephone2")
        );
        return customer;
    }

}
