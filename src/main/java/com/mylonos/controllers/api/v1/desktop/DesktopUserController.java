package com.mylonos.controllers.api.v1.desktop;

import com.mylonos.dao.impl.UserDAOImpl;
import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import com.mylonos.psinter.domain.dto.UserCurrentPositionDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/desktop")
@CrossOrigin
class DesktopUserController {

    @Autowired
    private UserDAOImpl userDAO;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public AuthenticationDTO loginUser(@RequestParam(value = "username", defaultValue = "World") String username, @RequestParam(value = "password", defaultValue = "World") String password) {
        return userDAO.authenticate(username, password);
    }

    @CrossOrigin
    @RequestMapping(value = "/user1", method = RequestMethod.GET)
    public String getUsernamea(@RequestParam(value = "name", defaultValue = "World") String name) {
        return userDAO.getUsername(name);
    }

    @CrossOrigin
    @RequestMapping("/users_current_position")
    public List<UserCurrentPositionDTO> getAllUsersCurrentPosition() {
        return userDAO.getUsersCurrentPosition();
    }

    public void setUserDAO(UserDAOImpl userDAO) {
        this.userDAO = userDAO;
    }
}
