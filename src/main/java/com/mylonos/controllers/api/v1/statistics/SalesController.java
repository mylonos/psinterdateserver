package com.mylonos.controllers.api.v1.statistics;

import com.mylonos.processors.LineDataResults;
import com.mylonos.processors.LineDataResultsHightChart;
import com.mylonos.psinter.domain.dto.highchart.LineDataHC;
import com.mylonos.psinter.domain.dto.highchart.LineDataHCDto;
import com.mylonos.psinter.domain.dto.statistics.LineDataDTO;
import com.mylonos.psinter.domain.models.statistics.Query;
import com.mylonos.psinter.domain.models.statistics.QueryUtils;
import com.mylonos.service.StatisticService;
import com.mylonos.utils.Constants;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/desktop/statistics")
@CrossOrigin
class SalesController {

    @Autowired
    private StatisticService statisticService;
//    @Autowired
//    private CustomerDAO customerDAO;
    private final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    private final SimpleDateFormat monthFormatter = new SimpleDateFormat("MM-dd");
    private final SimpleDateFormat dayFormatter = new SimpleDateFormat("MM-dd");
    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = "/sales_comparison", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public LineDataHCDto salesComparison(@RequestBody Query query) {

        HashMap results = new HashMap();

        String url = Constants.ELS_END_POINT;

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        String[] response = new String[years.size()];
        int x = 0;
        OkHttpClient client = new OkHttpClient();

        QueryUtils qu = new QueryUtils();
        String companies = qu.getQueryTerms(query.getCompanies(), "companyCode");
        String customers = qu.getQueryTerms(query.getCustomers(), "accountId");
        String docTypes = qu.getQueryTerms(query.getDocuments(), "documentTypeCode");

        for (long year : years) {
            String from = year + "-" + monthFormatter.format(query.getFrom());
            String until = year + "-" + monthFormatter.format(query.getUntil());
            String q = ""
                    + "{\n"
                    + "  \"query\": {\n"
                    + "    \"bool\": {\n"
                    + "      \"must\": [\n"
                    + "        {\n"
                    + "          \"term\": {\n"
                    + "            \"cancel\": \"0\"\n"
                    + "          }\n"
                    + "        },\n"
                    + "       \n"
                    + "        {\n"
                    + "          \"bool\": {\n"
                    + "            \"should\": [" + docTypes + "]\n"
                    + "          }\n"
                    + "        },\n"
                    + "        {\n"
                    + "          \"bool\": {\n"
                    + "            \"should\": [" + companies + "]\n"
                    + "          }\n"
                    + "        },\n"
                    + "        {\n"
                    + "          \"range\": {\n"
                    + "            \"createdDate\": {\n"
                    + "              \"format\": \"yyyy-MM-dd\",\n"
                    + "              \"from\": \"" + from + "\",\n"
                    + "              \"to\": \"" + until + "\"\n"
                    + "            }\n"
                    + "          }\n"
                    + "        }\n"
                    + "      ],\n"
                    + "      \"must_not\": [\n"
                    + "      ],\n"
                    + "      \"should\": [\n"
                    + "      ]\n"
                    + "    }\n"
                    + "  },\n"
                    + ""
                    + "  \"size\": 0,\n"
                    + "  \"aggs\": {\n"
                    + "    \"period\": {\n"
                    + "      \"date_histogram\": {\n"
                    + "        \"field\": \"createdDate\",\n"
                    + "        \"interval\": \"" + query.getInterval() + "\",\n"
                    + "        \"format\": \"yyyy-MM-dd\",\n"
                    + "        \"extended_bounds\" : {\n"
                    + "                    \"min\" : \"" + from + "\"\n"
                    + "                }"
                    + "      },\n"
                    + "      \"aggs\": {\n"
                    + "        \"total\": {\n"
                    + "          \"sum\": {\n"
                    + "            \"field\": \"payableAmount\"\n"
                    + "          }\n"
                    + "        }\n"
                    + "      }\n"
                    + "    }\n"
                    + "  }\n"
                    + "}";

            okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, q);
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic ZWxhc3RpYzpjaGFuZ2VtZQ==")
                    .post(body)
                    .build();
//            System.out.println("=================");
//            System.out.println(q);
//            System.out.println("=================");
            // create a thread for this request
            try {
                response[x++] = client.newCall(request).execute().body().string();
                results.put(year, client.newCall(request).execute().body().string());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new LineDataResultsHightChart().createLineDataDTO(years, response);
    }

    @RequestMapping(value = "/sales_comparison_for_quantity", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public LineDataHCDto salesComparisonForQuantity(@RequestBody Query query) {

        String url = Constants.ELS_END_POINT_PRODUCTS;

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        String[] response = new String[years.size()];
        int x = 0;
        OkHttpClient client = new OkHttpClient();
        QueryUtils qu = new QueryUtils();
        String companies = qu.getQueryTerms(query.getCompanies(), "companyCode");
        String customers = qu.getQueryTerms(query.getCustomers(), "accountId");
        String docTypes = qu.getQueryTerms(query.getDocuments(), "documentTypeCode");

        for (long year : years) {
            String from = year + "-" + monthFormatter.format(query.getFrom());
            String until = year + "-" + monthFormatter.format(query.getUntil());
            String q
                    = "{\n"
                    + "  \"query\": {\n"
                    + "    \"bool\": {\n"
                    + "      \"must\": [\n"
                    + "        {\n"
                    + "          \"term\": {\n"
                    + "            \"cancel\": \"0\"\n"
                    + "          }\n"
                    + "        },\n"
                    + "       \n"
                    + "        {\n"
                    + "          \"bool\": {\n"
                    + "            \"should\": [" + docTypes + "]\n"
                    + "          }\n"
                    + "        },\n"
                    + "        {\n"
                    + "          \"bool\": {\n"
                    + "            \"should\": [" + companies + "]\n"
                    + "          }\n"
                    + "        },\n"
                    + "        {\n"
                    + "          \"range\": {\n"
                    + "            \"createdDate\": {\n"
                    + "              \"format\": \"yyyy-MM-dd\",\n"
                    + "              \"from\": \"" + from + "\",\n"
                    + "              \"to\": \"" + until + "\"\n"
                    + "            }\n"
                    + "          }\n"
                    + "        }\n"
                    + "      ],\n"
                    + "      \"must_not\": [\n"
                    + "      ],\n"
                    + "      \"should\": [\n"
                    + "      ]\n"
                    + "    }\n"
                    + "  },\n"
                    + "  \"sort\": [ ],"
                    + ""
                    + ""
                    + ""
                    + ""
                    + ""
                    + "  \"size\": 0,\n"
                    + "  \"aggs\": {\n"
                    + "    \"period\": {\n"
                    + "      \"date_histogram\": {\n"
                    + "        \"field\": \"createdDate\",\n"
                    + "        \"interval\": \"" + query.getInterval() + "\",\n"
                    + "        \"format\": \"yyyy-MM-dd\",\"min_doc_count\": 0,\n"
                    + "        \"extended_bounds\" : {\n"
                    + "                    \"min\" : \"" + from + "\"\n"
                    + "                }"
                    + "      },\n"
                    + "      \"aggs\": {\n"
                    + "        \"total\": {\n"
                    + "          \"sum\": {\n"
                    + "            \"field\": \"payableAmount\"\n"
                    + "          }\n"
                    + "        }\n"
                    + "      }\n"
                    + "    }\n"
                    + "  }\n"
                    + "}";

            System.out.println(q);
            okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, q);
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Content-Type", "application/json")
                    //                    .addHeader("Authorization", "Basic ZWxhc3RpYzpjaGFuZ2VtZQ==")
                    .post(body)
                    .build();
//            System.out.println("=================");
//            System.out.println(q);
//            System.out.println("=================");

            //TODO create a thread for this request
            try {
                response[x++] = client.newCall(request).execute().body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new LineDataResultsHightChart().createLineDataDTO(years, response);
    }

    @RequestMapping(value = "/openings_per_month", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public LineDataHCDto openningsPerMonthPerCompany(@RequestBody Query query) {

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        String[] response = new String[years.size()];
        int x = 0;
        OkHttpClient client = new OkHttpClient();
        ArrayList<String> companies = query.getCompanies();
        ArrayList<LineDataHC> data = new ArrayList();
        LineDataHCDto lineDataHCDto = new LineDataHCDto();

        for (String company : companies) {
            data.add(statisticService.calculateStatsForOpeningsPerMonthPerStore(company, df.format(query.getFrom()), df.format(query.getUntil())));
        }
        if (!data.isEmpty()) {
            data.get(0).getExtra().get("labels");
        }
        lineDataHCDto.setLabels((ArrayList<String>) data.get(0).getExtra().get("labels"));
        lineDataHCDto.setSeries(data);

        return lineDataHCDto;
    }

    @RequestMapping(value = "/sales_per_month", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public LineDataHCDto salesPerMonthPerCompany(@RequestBody Query query) {

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        String[] response = new String[years.size()];
        int x = 0;
        OkHttpClient client = new OkHttpClient();
        ArrayList<String> companies = query.getCompanies();

        ArrayList<LineDataHC> data = new ArrayList();
        LineDataHCDto lineDataHCDto = new LineDataHCDto();

        for (Long year : years) {
            data.add(statisticService.calculateSalesStatsForPerMonthPerStore(companies.get(0), year + "-" + dayFormatter.format(query.getFrom()), year + "-" + dayFormatter.format(query.getUntil()), "1M", year));
        }
        if (!data.isEmpty()) {
            data.get(0).getExtra().get("labels");
        }
        lineDataHCDto.setLabels((ArrayList<String>) data.get(0).getExtra().get("labels"));
        lineDataHCDto.setSeries(data);

        return lineDataHCDto;
    }

    @RequestMapping(value = "/customer_sales_comparison", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public LineDataHCDto salesPerCustomerYearComparison(@RequestBody Query query) {

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        String[] response = new String[years.size()];
        int x = 0;

        ArrayList<LineDataHC> data = new ArrayList();
        LineDataHCDto lineDataHCDto = new LineDataHCDto();

        for (Long year : years) {
            data.add(statisticService.calculateSalesStatsPerCustomer(query.getCustomer(), year + "-" + dayFormatter.format(query.getFrom()), year + "-" + dayFormatter.format(query.getUntil()), "1W", year + ""));
        }
        if (!data.isEmpty()) {
            data.get(0).getExtra().get("labels");
        }
        lineDataHCDto.setLabels((ArrayList<String>) data.get(0).getExtra().get("labels"));
        lineDataHCDto.setSeries(data);

        return lineDataHCDto;
    }

    @RequestMapping(value = "/quantities_per_week", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public LineDataHCDto quantitiesPerWeekPerCompany(@RequestBody Query query) {

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        String[] response = new String[years.size()];
        int x = 0;
        OkHttpClient client = new OkHttpClient();
        ArrayList<String> companies = query.getCompanies();

        ArrayList<LineDataHC> data = new ArrayList();
        LineDataHCDto lineDataHCDto = new LineDataHCDto();

        for (String company : companies) {
            data.add(statisticService.calculateQuantitiesStatsPerWeekPerStore(company, df.format(query.getFrom()), df.format(query.getUntil()), "1M", 2016L, "KIV"));
        }
        if (!data.isEmpty()) {
            data.get(0).getExtra().get("labels");
        }
        lineDataHCDto.setLabels((ArrayList<String>) data.get(0).getExtra().get("labels"));
        lineDataHCDto.setSeries(data);

        return lineDataHCDto;
    }

    @RequestMapping(value = "/sales_per_week", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public LineDataHCDto salesPerWeekPerCompany(@RequestBody Query query) {

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        String[] response = new String[years.size()];
        int x = 0;
        OkHttpClient client = new OkHttpClient();
        ArrayList<String> companies = query.getCompanies();

        ArrayList<LineDataHC> data = new ArrayList();
        LineDataHCDto lineDataHCDto = new LineDataHCDto();

//        for (Long year : years) {
//            data.add(statsCalulator.calculateSalesStatsPerWeekPerStore(companies.get(0), year + "-" + dayFormatter.format(query.getFrom()), year + "-" + dayFormatter.format(query.getUntil()), "1M", year, "KIV"));
//        }
//
        for (String company : companies) {
            data.add(statisticService.calculateSalesStatsPerWeekPerStore(company, df.format(query.getFrom()), df.format(query.getUntil()), "1M", 2016L, "KIV"));
        }
        if (!data.isEmpty()) {
            data.get(0).getExtra().get("labels");
        }
        lineDataHCDto.setLabels((ArrayList<String>) data.get(0).getExtra().get("labels"));
        lineDataHCDto.setSeries(data);

        return lineDataHCDto;
    }

    @RequestMapping(value = "/sales_per_store_and_product", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public HashMap<String, Object> salesStatsPerStorePerProduct(@RequestBody Query query) {

        ArrayList<Long> years = (ArrayList<Long>) query.getYears();
        Long l = 2016L;
        ArrayList<String> companies = query.getCompanies();

        ArrayList<LineDataHC> data = new ArrayList();
        LineDataHCDto lineDataHCDto = new LineDataHCDto();

        return statisticService.calculateSalesStatsPerStorePerProduct(companies.get(0), df.format(query.getFrom()),
                df.format(query.getUntil()), query.getInterval(), l, query.getUnitOfMeasure(), query.getOrder());
    }

    @RequestMapping(value = "/sales_for_two_dates", method = RequestMethod.GET)
    public LineDataDTO sales_for_two_dates(@RequestParam(value = "years", defaultValue = "{0}") int[] years,
            @RequestParam(value = "from", defaultValue = "2016-01-01") String from,
            @RequestParam(value = "until", defaultValue = "2016-12-31") String until,
            @RequestParam(value = "from1", defaultValue = "2016-01-01") String from1,
            @RequestParam(value = "until1", defaultValue = "2016-12-31") String until1,
            @RequestParam(value = "company", defaultValue = "[1000]") String[] company,
            @RequestParam(value = "interval", defaultValue = "1M") String interval
    ) {

        years = new int[]{1, 2};

        Date startDate = null;
        Date endDate = null;
        try {
            startDate = df.parse(from);
            endDate = df.parse(until);
//            String newDateString = df.format(startDate);
//            System.out.println(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String fromD = monthFormatter.format(startDate);
        String untilD = monthFormatter.format(endDate);
        System.out.println();

        OkHttpClient client = new OkHttpClient();
        int x = 0;
        String[] response = new String[years.length];
        String url = Constants.ELS_END_POINT;
        for (int i : years) {

            if (i == 1) {
                try {
                    startDate = df.parse(from);
                    endDate = df.parse(until);
                    fromD = from;
                    untilD = until;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    startDate = df.parse(from1);
                    endDate = df.parse(until1);
                    fromD = from1;
                    untilD = until1;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            String query = "{\n"
                    + "  \"query\": {\n"
                    + "    \"bool\": {\n"
                    + "      \"must\": [\n"
                    + "        \n"
                    + "          {\n"
                    + "\"term\": {\n"
                    + "\"cancel\": \"0\"\n"
                    + "}\n"
                    + "}\n"
                    + ",\n"
                    + "{\n"
                    + "\"term\": {\n"
                    + "\"documentTypeCode\": \"ΤΔΑ\"\n"
                    + "}\n"
                    + "\n"
                    + "        },\n"
                    + "        {\n"
                    + "          \"range\": {\n"
                    + "            \"createdDate\": {\n"
                    + "              \"format\": \"yyyy-MM-dd\",\n"
                    + "              \"from\": \"" + fromD + "\",\n"
                    + "              \"to\": \"" + untilD + "\"   \n"
                    + "            }\n"
                    + "          }\n"
                    + "        }\n"
                    + "      ]\n"
                    + "    }\n"
                    + "  },\n"
                    + "  \"size\": 0,\n"
                    + "  \"aggs\": {\n"
                    + "    \"period\": {\n"
                    + "      \"date_histogram\": {\n"
                    + "        \"field\": \"createdDate\",\n"
                    + "        \"interval\": \"" + interval + "\",\n"
                    + "        \"format\": \"yyyy-MM-dd\",\n"
                    + "        \"min_doc_count\": 0\n"
                    + "      },\n"
                    + "      \"aggs\": {\n"
                    + "        \"total\": {\n"
                    + "          \"sum\": {\n"
                    + "            \"field\": \"payableAmount\"\n"
                    + "          }\n"
                    + "        }\n"
                    + "      }\n"
                    + "    }\n"
                    + "  }\n"
                    + "}";

            okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, query);
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic ZWxhc3RpYzpjaGFuZ2VtZQ==")
                    .post(body)
                    .build();

            // create a thread for this request
            try {
                response[x++] = client.newCall(request).execute().body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return new LineDataResults().createLineDataDTO(response);
    }

}
