package com.mylonos.controllers.api.v1.migration;

import com.mylonos.dao.CustomerDAO;
import com.mylonos.psinter.domain.dto.CustomerDTO;
import okhttp3.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/migration")
class CustomerController {

    @Autowired
    private CustomerDAO customerDAO;
    private final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    @CrossOrigin
    @RequestMapping(value = "/customers", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public boolean insertCustomers(@RequestBody CustomerDTO cdto) {

        if (cdto == null) {
            return false;
        }
        return customerDAO.isertCustomers(cdto);
    }

}
