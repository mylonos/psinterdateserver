package com.mylonos.controllers.api.v1.desktop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylonos.dao.CustomerDAO;
import com.mylonos.psinter.domain.dto.CustomerDTO;
import com.mylonos.psinter.domain.dto.CustomerStatusDTO;
import com.mylonos.psinter.domain.dto.CustomerVisitDTO;
import com.mylonos.psinter.domain.dto.CustomersOnMapResults;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.CustomerSimple;
import com.mylonos.psinter.domain.models.CustomerSimpleInvoices;
import com.mylonos.utils.DateHelper;
import com.mylonos.utils.GZIPCompression;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/desktop")
class DesktopCustomerController {

    @Autowired
    private CustomerDAO customerDAO;

    @CrossOrigin
    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public CustomerDTO getAllCustomers(@RequestParam(value = "q", defaultValue = "") String q) {
        return customerDAO.getAllCustomers(q);
    }

    @CrossOrigin
    @RequestMapping(value = "/customers_b", method = RequestMethod.GET)
    public byte[] getAllCustomersB(@RequestParam(value = "q", defaultValue = "") String q) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        String data = mapper.writeValueAsString(customerDAO.getAllCustomers(q));
        GZIPCompression gzipCompression = new GZIPCompression();

        return gzipCompression.compress(data);
    }

    @CrossOrigin
    @RequestMapping(value = "/customers_simple", method = RequestMethod.GET)
    public List<CustomerSimple> getAllCustomersSimple(@RequestParam(value = "q", defaultValue = "") String q,
            @RequestParam(value = "limit", defaultValue = "1000") int limit) {
        return customerDAO.getAllCustomersSimple(q, limit);
    }

    @CrossOrigin
    @RequestMapping(value = "/customer_simple", method = RequestMethod.GET)
    public CustomerSimple getCustomerSimple(@RequestParam(value = "id", defaultValue = "") String id) {
        return customerDAO.getCustomerSimple(id);
    }

    @CrossOrigin
    @RequestMapping(value = "/customer_simple_invoices", method = RequestMethod.GET)
    public CustomerSimpleInvoices getCustomerSimpleInvoices(@RequestParam(value = "id", defaultValue = "") String id) {
        return customerDAO.getCustomerSimpleInvoices(id);
    }

    @CrossOrigin
    @RequestMapping(value = "/customer_invoices", method = RequestMethod.GET)
    public Results getCustomerInvoices(@RequestParam(value = "id", defaultValue = "") String id,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "resultsPerPage", defaultValue = "10000") Integer resultsPerPage) {
        return customerDAO.getCustomerInvoices(id, page, resultsPerPage);
    }

    @CrossOrigin
    @RequestMapping(value = "/customers_by_company", method = RequestMethod.GET)
    public CustomerDTO getAllCustomersByCompany(@RequestParam(value = "q", defaultValue = "") String q,
            @RequestParam(value = "id", defaultValue = "") String id
    ) {
        return customerDAO.getAllCustomersByCompany(id, q);
    }

    @CrossOrigin
    @RequestMapping(value = "/customers_map", method = RequestMethod.GET)
    public CustomersOnMapResults getAllCustomersForMap(@RequestParam(value = "id", defaultValue = "") String company) {
        return customerDAO.getAllCustomersForCompany(company);
    }

    @CrossOrigin
    @RequestMapping(value = "/customers_visits_for_map", method = RequestMethod.GET)
    public List<CustomerVisitDTO> getAllCustomersVistisForMap(@RequestParam(value = "user", defaultValue = "") String user_id, @RequestParam(value = "d", defaultValue = "") String date) {

        DateHelper dateHelper = new DateHelper();
        System.out.println(dateHelper.getGrDateInMillis(date));
        System.out.println(dateHelper.getGrDateNextInMillis(date));

        return customerDAO.getAllCustomersForVistis(user_id, dateHelper.getGrDateInMillis(date), dateHelper.getGrDateNextInMillis(date));
    }

    @CrossOrigin
    @RequestMapping(value = "/customer_status", method = RequestMethod.GET)
    public CustomerStatusDTO getCustomersStatus(@RequestParam(value = "store", defaultValue = "") String store) {

        return customerDAO.getCustomerStatus(store);
    }

}
