package com.mylonos.controllers.api.v1.mobile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylonos.dao.CurrentStatusDAO;
import com.mylonos.psinter.domain.dto.CurrentStatusDTO;
import com.mylonos.psinter.domain.models.CurrentPotition;
import com.mylonos.utils.GZIPCompression;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/mobile")
public class CurrentStatusController {

    @Autowired
    private CurrentStatusDAO currentStatus;

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/current_status", method = RequestMethod.POST)
    public void updateCurrentStatus(@RequestBody CurrentStatusDTO currentStatusDTO) {
        currentStatus.importCurrentStatus(currentStatusDTO);

//        System.out.println("----------------------------");
//        System.out.println(currentStatusDTO.getDevice_id());
//        System.out.println(currentStatusDTO.getCurrentPotition().getLatitude());
//        System.out.println(currentStatusDTO.getCurrentPotition().getLongitude());
//        System.out.println(currentStatusDTO.getCurrentPotition().getSpeed());
//        System.out.println(currentStatusDTO.getDevice_id());
//        System.out.println(currentStatusDTO.getUser_id());
//
//        List<CustomerVisitDTO> customerVisitDTOs = currentStatusDTO.getCustomerVisitDTO();
//        for (CustomerVisitDTO customerVisitDTO : customerVisitDTOs) {
//            System.out.println("*****************************************");
//
//            System.out.println(customerVisitDTO.getName());
//            System.out.println(customerVisitDTO.getReference());
//            System.out.println(customerVisitDTO.getAddress1());
//            System.out.println(customerVisitDTO.getVisit_start_date());
//            System.out.println(customerVisitDTO.getVisit_finish_date());
//            System.out.println(customerVisitDTO.getDuration());
//            System.out.println("*****************************************");
//
//
//        }
//        System.out.println("----------------------------");
//        return routeDAO.getRoutes();
    }

    @CrossOrigin
    @RequestMapping(value = "/current_status_b", method = RequestMethod.POST)
    public void updateCurrentStatusBt(@RequestBody byte[] bytes) throws IOException {
        GZIPCompression gzipCompression = new GZIPCompression();
        ObjectMapper mapper = new ObjectMapper();
        CurrentStatusDTO currentStatusDTO = mapper.readValue(gzipCompression.decompress(bytes), CurrentStatusDTO.class);
        currentStatus.importCurrentStatus(currentStatusDTO);
    }

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/current_position", method = RequestMethod.POST)
    public void setCurrentPosition(@RequestBody CurrentPotition currentPotition) {

//
//        System.out.println("----------------------------------------------------");
//        System.out.println(currentPotition.getLatitude());
//        System.out.println(currentPotition.getLongitude());
//        System.out.println(currentPotition.getSpeed());
//        System.out.println(currentPotition.getUser_id());
//        System.out.println(currentPotition.getDevice_id());
//        System.out.println("----------------------------------------------------");
        if (currentPotition.getUser_id() != null) {
            currentStatus.setUserPosition(currentPotition);
        }

    }

}
