package com.mylonos.controllers.api.v1.desktop;

import com.mylonos.dao.VehicleDao;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Vehicle;
import com.mylonos.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/desktop")
class VehicleController {

    @Autowired
    private VehicleDao vehicleDao;

    @CrossOrigin
    @RequestMapping(value = "/vehicles", method = RequestMethod.GET)
    public Results getAllCustomers(@RequestParam(value = "store", defaultValue = "") String store) {
        return vehicleDao.getVehicleByStore(store);
    }

    @CrossOrigin
    @RequestMapping(value = "/vehicle",
            method = RequestMethod.POST, consumes = "application/json")
    public void createVehicle(@RequestBody Vehicle vehicle) {

        if (null == vehicle.getVehicleId() || vehicle.getVehicleId().trim().isEmpty()) {
            vehicle.setVehicleId(new Utils().getUniqueId());
            vehicleDao.createVehicle(vehicle);
        } else {
            vehicleDao.updateVehicle(vehicle);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/vehicle",
            method = RequestMethod.DELETE)
    public void deleteVehicle(@RequestParam(value = "id", defaultValue = "") String id) {

        vehicleDao.deleteVehicle(id);

    }

}
