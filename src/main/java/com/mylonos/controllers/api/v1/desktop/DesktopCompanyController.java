package com.mylonos.controllers.api.v1.desktop;

import com.mylonos.dao.CompanyDAO;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/desktop")
class DesktopCompanyController {

    @Autowired
    private CompanyDAO companyDAO;

    @CrossOrigin
    @RequestMapping(value = "/companies", method = RequestMethod.GET)
    public Results getAllCompanies(@RequestParam(value = "q", defaultValue = "") String q,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "resultsPerPage", defaultValue = "10000") Integer limit) {

        return companyDAO.getAllCompanies(q, page, limit);
    }

    @CrossOrigin
    @RequestMapping(value = "/company", method = RequestMethod.PUT)
    public void updateCompany(@RequestParam(value = "company", defaultValue = "") Company company) {
        companyDAO.updateCompany(company);
    }

}
