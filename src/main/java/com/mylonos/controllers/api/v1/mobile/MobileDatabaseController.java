package com.mylonos.controllers.api.v1.mobile;

import com.mylonos.dao.MobileDatabaseDAO;
import com.mylonos.psinter.domain.models.MobileDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/mobile")
class MobileDatabaseController {

    @Autowired
    private MobileDatabaseDAO maintenanceDAO;

    @CrossOrigin
    @RequestMapping(value = "/db", method = RequestMethod.GET)
    public MobileDatabase getAllMaintenances() {
        return maintenanceDAO.getmobileDatabase();
    }


}
