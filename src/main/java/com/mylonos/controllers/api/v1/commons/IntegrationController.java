package com.mylonos.controllers.api.v1.commons;

import com.mylonos.dao.impl.CustomerCategoryDAOImpl;
import com.mylonos.psinter.domain.dto.CustomerCategoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by manolis on 12/30/2015.
 */
@RestController
@RequestMapping("/api/v1")
public class IntegrationController {

    @Autowired
    private CustomerCategoryDAOImpl customerCategoryDAO;

    public void setCustomerCategoryDAO(CustomerCategoryDAOImpl customerCategoryDAO) {
        this.customerCategoryDAO = customerCategoryDAO;
    }

    @CrossOrigin
    @RequestMapping(value = "/list_company_types", method = RequestMethod.POST)
    public CustomerCategoryDTO getAllCustomerCategories() {

        CustomerCategoryDTO customerCategoryDTO = customerCategoryDAO.getAllCustomerCategories();
        customerCategoryDTO.getCustomerCategories().forEach(customerCategory -> {

            System.out.println(customerCategory.getCustomerCategoryName());

            customerCategoryDAO.updateCustomerCategory(customerCategory.getCustomerCategoryId(), customerCategory.getCustomerCategoryName());

        });
        return customerCategoryDAO.getAllCustomerCategories();
    }

}
