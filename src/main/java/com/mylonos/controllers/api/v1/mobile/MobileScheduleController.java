package com.mylonos.controllers.api.v1.mobile;

import com.mylonos.controllers.api.v1.*;
import com.mylonos.dao.CustomerDAO;
import com.mylonos.dao.RouteDAO;
import com.mylonos.dao.ScheduleDAO;
import com.mylonos.dao.impl.CustomerDAOImpl;
import com.mylonos.dao.impl.UserDAOImpl;
import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import com.mylonos.psinter.domain.dto.CustomerDTO;
import com.mylonos.psinter.domain.dto.ScheduleDTO;
import com.mylonos.psinter.domain.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/mobile")
class MobileScheduleController {

    @Autowired
    private ScheduleDAO scheduleDAO;

    @CrossOrigin
    @RequestMapping(value = "/schedules", method = RequestMethod.GET)
    public List<ScheduleDTO> getAllCustomers() {
        return scheduleDAO.getSchedules("");
    }

    
}
