package com.mylonos.controllers.api.v1.desktop;

import com.mylonos.dao.TemperatureDAO;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Temperature;
import com.mylonos.utils.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/desktop")
public class DesktopTemperatureController {

    @Autowired
    private TemperatureDAO temperatureDAO;

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/get_temperatures_for_vehicle", method = RequestMethod.GET)
    public Results getTemperaturesForVehicle(
            @RequestParam(value = "vehicle_id",
                    defaultValue = "") String vehicleId,
            @RequestParam(value = "date", defaultValue = "") String date,
            @RequestParam(value = "date_until", defaultValue = "") String dateUntil
    ) {
        DateHelper dateHelper = new DateHelper();

        return temperatureDAO.getTemperaturesForVehicle(vehicleId, dateHelper.getGrDateInMillis(date), dateHelper.getGrDateNextInMillis(dateUntil));

    }

//    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/get_last_temperature_for_vehicle", method = RequestMethod.GET)
    public Temperature getLastTemperatureForVehicle(@RequestParam(value = "vehicle_id", defaultValue = "") String vehicleId, @RequestParam(value = "date", defaultValue = "") String date) {

        return temperatureDAO.getLastTemperatureByVehicleId(vehicleId);

    }

}
