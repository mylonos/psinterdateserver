package com.mylonos.controllers.api.v1.desktop;

import com.mylonos.dao.RouteDAO;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.Route;
import com.mylonos.utils.DateHelper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/desktop")
public class DesktopRoutesController {

    @Autowired
    private RouteDAO routeDAO;

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/get_routes", method = RequestMethod.GET)
    public List<Route> getAllRoutesFor(@RequestParam(value = "user_id", defaultValue = "") String user_id, @RequestParam(value = "date", defaultValue = "") String date) {
        DateHelper dateHelper = new DateHelper();

        return routeDAO.getRoutes(user_id, dateHelper.getGrDateInMillis(date), dateHelper.getGrDateNextInMillis(date));

    }

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/get_routes_by_vehicle", method = RequestMethod.GET)
    public Results getAllRoutesByVehicleFor(@RequestParam(
            value = "vehicle_id",
            defaultValue = "") String vehicleId,
            @RequestParam(value = "date", defaultValue = "") String date,
            @RequestParam(value = "date_until", defaultValue = "") String dateUntil
    ) {
        DateHelper dateHelper = new DateHelper();

        return routeDAO.getRoutesByVehicle(vehicleId, dateHelper.getGrDateInMillis(date), dateHelper.getGrDateNextInMillis(dateUntil));

    }

    @CrossOrigin
    @RequestMapping(value = "/get_routes_info_by_vehicle", method = RequestMethod.GET)
    public Results getAllRoutesInfoByVehicleFor(@RequestParam(
            value = "vehicle_id",
            defaultValue = "") String vehicleId,
            @RequestParam(value = "date", defaultValue = "") String date,
            @RequestParam(value = "date_until", defaultValue = "") String dateUntil
    ) {
        DateHelper dateHelper = new DateHelper();

        return routeDAO.getRoutesInfoByVehicle(vehicleId, dateHelper.getGrDateInMillis(date), dateHelper.getGrDateNextInMillis(dateUntil));

    }

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/get_route_by_id", method = RequestMethod.GET)
    public Route getRouteById(@RequestParam(value = "route_id", defaultValue = "") String routeId) {
        return routeDAO.getRoutesByRouteId(routeId);
    }

}
