package com.mylonos.controllers.api.v1.desktop;

import com.google.common.base.Strings;
import com.mylonos.dao.UserDAO;
import com.mylonos.psinter.domain.dto.Results;
import com.mylonos.psinter.domain.models.User;
import com.mylonos.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/desktop")
class EmployeeController {

    @Autowired
    private UserDAO userDao;

    @CrossOrigin
    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    public Results getAllCustomers(@RequestParam(value = "store", defaultValue = "") String store) {
        return userDao.findByEmployeeByStore(store, 1, 1);
    }

    @CrossOrigin
    @RequestMapping(value = "/employee", method = RequestMethod.POST, consumes = "application/json")
    public void createEmployee(@RequestBody User user) {

        if (!Strings.isNullOrEmpty(user.getUserId())) {
            //update user
            userDao.updateUser(user);
        } else {
//            insert user
            user.setUserId(new Utils().getUniqueId());
            user.setIsActive(1);
            userDao.createUser(user);
        }

    }

    @CrossOrigin
    @RequestMapping(value = "/employee", method = RequestMethod.DELETE)
    public void deleteEmployee(@RequestParam(value = "id", defaultValue = "") String id) {

        userDao.deleteUser(id);

    }

}
