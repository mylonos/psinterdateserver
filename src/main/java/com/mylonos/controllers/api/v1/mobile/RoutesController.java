package com.mylonos.controllers.api.v1.mobile;

import com.mylonos.dao.RouteDAO;
import com.mylonos.psinter.domain.dto.RoutesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/mobile")
public class RoutesController {

    @Autowired
    private RouteDAO routeDAO;

    @CrossOrigin
    @RequestMapping(value = "/routes", method = RequestMethod.POST)
    public void getAllRoutes(@RequestBody RoutesDTO routesDTO) {

//        routeDAO.insertRoutes(routesDTO);
    }

}
