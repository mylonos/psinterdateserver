package com.mylonos.controllers.api.v1.commons;

import com.mylonos.dao.impl.UserDAOImpl;
import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserDAOImpl userDAO;

    @CrossOrigin
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public AuthenticationDTO loginUser(@RequestParam(value = "username", defaultValue = "World") String username, @RequestParam(value = "password", defaultValue = "World") String password) {
        return userDAO.authenticate(username, password);
    }

    public void setUserDAO(UserDAOImpl userDAO) {
        this.userDAO = userDAO;
    }
}
