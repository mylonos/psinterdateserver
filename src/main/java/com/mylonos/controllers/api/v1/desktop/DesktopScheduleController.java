package com.mylonos.controllers.api.v1.desktop;

import com.mylonos.dao.ScheduleDAO;
import com.mylonos.psinter.domain.dto.ScheduleDesktopDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/desktop")
class DesktopScheduleController {

    @Autowired
    private ScheduleDAO scheduleDAO;

    @CrossOrigin
    @RequestMapping(value = "/schedules", method = RequestMethod.GET)
    public List<ScheduleDesktopDTO> getAllSchedules(@RequestParam(value = "store",
            defaultValue = "") String store) {
        return scheduleDAO.getSchedulesWithDetails(store);
    }

    @CrossOrigin
    @RequestMapping(value = "/schedules_item", method = RequestMethod.GET)
    public List<ScheduleDesktopDTO> getAllScheduleItems(@RequestParam(value = "store",
            defaultValue = "") String store) {
        return scheduleDAO.getSchedulesWithDetails(store);
    }

    @CrossOrigin
    @RequestMapping(value = "/create_schedule", method = RequestMethod.POST)
    public ScheduleDesktopDTO createSchedule(@RequestParam(value = "title") String title, @RequestParam(value = "store",
            defaultValue = "") String store, @RequestParam(value = "user_id", defaultValue = "") String user_id) {
        return scheduleDAO.createSchedule(title, store, user_id);
    }

    @CrossOrigin
    @RequestMapping(value = "/add_schedule_point", method = RequestMethod.POST)
    public void addPoint(@RequestParam(value = "schedule_id") String schedule_id, @RequestParam(value = "customer_id",
            defaultValue = "") String customer_id, @RequestParam(value = "position", defaultValue = "0") int position) {
        scheduleDAO.addCustomerToSchedule(schedule_id, customer_id, position);
    }

    @CrossOrigin
    @RequestMapping(value = "/remove_schedule_point", method = RequestMethod.POST)
    public void removePoint(@RequestParam(value = "schedule_id") String schedule_id, @RequestParam(value = "customer_id",
            defaultValue = "") String customer_id, @RequestParam(value = "position", defaultValue = "0") int position) {
        scheduleDAO.addCustomerToSchedule(schedule_id, customer_id, position);
    }

}
