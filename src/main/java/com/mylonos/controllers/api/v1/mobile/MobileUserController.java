package com.mylonos.controllers.api.v1.mobile;

import com.mylonos.dao.UserDAO;
import com.mylonos.dao.impl.UserDAOImpl;
import com.mylonos.psinter.domain.dto.AuthenticationDTO;
import com.mylonos.psinter.domain.models.MobileUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/mobile")
public class MobileUserController {

    @Autowired
    private UserDAO userDAO;

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public AuthenticationDTO loginUser(@RequestParam(value = "username", defaultValue = "World") String username, @RequestParam(value = "password", defaultValue = "World") String password) {
        return userDAO.authenticate(username, password);
    }

    @CrossOrigin
    @RequestMapping("/user")
    public List<MobileUser> getAllUsers() {
        return userDAO.findByUsernameMobile("");
    }

    public void setUserDAO(UserDAOImpl userDAO) {
        this.userDAO = userDAO;
    }
}
