package com.mylonos.controllers.api.v1.mobile;

import com.mylonos.dao.CustomerDAO;
import com.mylonos.psinter.domain.dto.CustomerDTO;
import com.mylonos.psinter.domain.models.Customer;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by manolis on 12/24/2015.
 */
@RestController
@RequestMapping("/api/v1/mobile")
public class MobileCustomerController {

    @Autowired
    private CustomerDAO customerDAO;

    @CrossOrigin
    @RequestMapping("/customer")
    public CustomerDTO getAllCustomers() {
        return customerDAO.getAllCustomers("");
    }

    @CrossOrigin
    @RequestMapping(value = "/set_customer_position", method = RequestMethod.POST)
    public void set_customer_position(@RequestBody Customer customer) {
        ArrayList<Customer> cl = new ArrayList<Customer>();
        cl.add(customer);
        customerDAO.updateCustomerPositions(cl);

    }

    //    @RequestMapping("/login")
    @CrossOrigin
    @RequestMapping(value = "/update_customer_positions", method = RequestMethod.POST)
    public void update_customer(@RequestBody List<Customer> customers) {

        customerDAO.updateCustomerPositions(customers);
    }

}
